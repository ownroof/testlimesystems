SELECT c.Id, c.INN, c.Lastname, c.Firstname, c.Middlename, c.Birthday, c.Gender, 
c.TypeId, t.Name AS TypeName, c.Address, c.RegionId, r.Name AS RegionName, c.IsActive 
FROM Clients AS c 
INNER JOIN Types AS t ON t.Id = c.TypeId
LEFT JOIN dbo.Regions AS r ON r.Id = c.RegionId
WHERE c.Id = 12