USE [TestClients2016]
GO

/****** Object:  Table [dbo].[ClientAttributes]    Script Date: 30.08.2016 18:20:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClientAttributes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttrId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
 CONSTRAINT [PK_ClientAttributes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClientAttributes]  WITH CHECK ADD  CONSTRAINT [FK_ClientAttributes_Attributes] FOREIGN KEY([AttrId])
REFERENCES [dbo].[Attributes] ([Id])
GO

ALTER TABLE [dbo].[ClientAttributes] CHECK CONSTRAINT [FK_ClientAttributes_Attributes]
GO

ALTER TABLE [dbo].[ClientAttributes]  WITH CHECK ADD  CONSTRAINT [FK_ClientAttributes_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
GO

ALTER TABLE [dbo].[ClientAttributes] CHECK CONSTRAINT [FK_ClientAttributes_Clients]
GO

