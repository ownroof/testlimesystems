USE [TestClients2016]
GO

/****** Object:  Table [dbo].[StringProperties]    Script Date: 29.08.2016 15:44:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StringProperties](
	[Id] [int] IDENTITY(2,1) NOT NULL,
	[ClientAttrId] [int] NOT NULL,
	[Value] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_StringProperties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StringProperties]  WITH CHECK ADD  CONSTRAINT [FK_StringProperties_ClientAttributes] FOREIGN KEY([ClientAttrId])
REFERENCES [dbo].[ClientAttributes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[StringProperties] CHECK CONSTRAINT [FK_StringProperties_ClientAttributes]
GO

