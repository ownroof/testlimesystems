USE [TestClients2016]
GO

/****** Object:  StoredProcedure [dbo].[RelatedClient_Insert]    Script Date: 31.08.2016 14:50:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RelatedClient_Insert]
   @ClientId INT,
   @AttrId INT,
   @RelatedClientId INT,
   @Id   INT OUTPUT,
   @ClientAttrId   INT OUTPUT
   AS 
   BEGIN TRY      
	   BEGIN TRAN
	   INSERT INTO dbo.ClientAttributes ( AttrId, ClientId )   VALUES  ( @AttrId, @ClientId)
	   SET @ClientAttrId = CAST(SCOPE_IDENTITY() AS INT)
	   INSERT INTO dbo.RelatedClient( ClientAttrId, RelatedClientId ) VALUES (@ClientAttrId, @RelatedClientId)
	   SET @Id = CAST(SCOPE_IDENTITY() AS INT)
	   COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH



GO

