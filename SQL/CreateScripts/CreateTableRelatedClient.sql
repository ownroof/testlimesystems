USE [TestClients2016]
GO

/****** Object:  Table [dbo].[RelatedClient]    Script Date: 31.08.2016 14:09:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RelatedClient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientAttrId] [int] NOT NULL,
	[RelatedClientId] [int] NOT NULL,
 CONSTRAINT [PK_RelatedClient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_RelatedClient] UNIQUE NONCLUSTERED 
(
	[ClientAttrId] ASC,
	[RelatedClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RelatedClient]  WITH CHECK ADD  CONSTRAINT [FK_RelatedClient_ClientAttributes] FOREIGN KEY([ClientAttrId])
REFERENCES [dbo].[ClientAttributes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RelatedClient] CHECK CONSTRAINT [FK_RelatedClient_ClientAttributes]
GO

ALTER TABLE [dbo].[RelatedClient]  WITH CHECK ADD  CONSTRAINT [FK_RelatedClient_Clients_Related] FOREIGN KEY([RelatedClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RelatedClient] CHECK CONSTRAINT [FK_RelatedClient_Clients_Related]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'advanced attributes for client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RelatedClient'
GO

