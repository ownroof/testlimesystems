USE [master]
GO
/****** Object:  Database [TestClients2016]    Script Date: 31.08.2016 16:42:27 ******/
CREATE DATABASE [TestClients2016]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TestClients2016', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQL\MSSQL\DATA\TestClients2016.mdf' , SIZE = 118784KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TestClients2016_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQL\MSSQL\DATA\TestClients2016_log.ldf' , SIZE = 1008000KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TestClients2016] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestClients2016].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestClients2016] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestClients2016] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestClients2016] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestClients2016] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestClients2016] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestClients2016] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestClients2016] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestClients2016] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestClients2016] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestClients2016] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestClients2016] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestClients2016] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestClients2016] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestClients2016] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestClients2016] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TestClients2016] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestClients2016] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestClients2016] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestClients2016] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestClients2016] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestClients2016] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestClients2016] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestClients2016] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TestClients2016] SET  MULTI_USER 
GO
ALTER DATABASE [TestClients2016] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestClients2016] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestClients2016] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestClients2016] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TestClients2016] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TestClients2016]
GO
/****** Object:  UserDefinedFunction [dbo].[ClearNonAlphaNumerical]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ClearNonAlphaNumerical] 
(
	-- Add the parameters for the function here
	@source NVARCHAR(50)
)
RETURNS NVARCHAR(50)
AS
BEGIN
	DECLARE @result NVARCHAR(50) = @source
	DECLARE @pos INT
	SET @pos = PATINDEX('%[^a-zA-Z0-9а-яА-Я ]%',@result)
-- sets @pos to the position of the first character
-- not equal to a letter or a digit (0-9)
 
WHILE @pos > 0 
BEGIN 
  SET @result = STUFF(@result,@pos,1,'')
  -- overwrites the non-alphanumeric character at position @pos with ''
  SET @pos = PATINDEX('%[^a-zA-Z0-9а-яА-Я ]%',@result)
  -- looks for the next non-alphanumeric character
END

	-- Return the result of the function
	RETURN @result

END

GO
/****** Object:  Table [dbo].[Attributes]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attributes](
	[Id] [int] IDENTITY(8,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[TypeId] [int] NOT NULL,
 CONSTRAINT [PK_Attributes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Attributes] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AttrTypes]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttrTypes](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsString] [bit] NOT NULL,
	[IsArray] [bit] NOT NULL,
	[IsReference] [bit] NOT NULL,
 CONSTRAINT [PK_AttrTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClientAttributes]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientAttributes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttrId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
 CONSTRAINT [PK_ClientAttributes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clients]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[INN] [nvarchar](10) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[Firstname] [nvarchar](50) NULL,
	[Middlename] [nvarchar](50) NULL,
	[Birthday] [date] NULL,
	[GenderId] [int] NULL,
	[TypeId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Clients_IsActive]  DEFAULT ((1)),
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Clients] UNIQUE NONCLUSTERED 
(
	[INN] ASC,
	[Lastname] ASC,
	[Firstname] ASC,
	[Middlename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CreateHistory]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreateHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Locale] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CreateHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dataAttributes]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dataAttributes](
	[Id] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[TypeId] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Genders]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genders](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Genders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ListProperties]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListProperties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientAttrId] [int] NOT NULL,
	[ValueId] [int] NOT NULL,
 CONSTRAINT [PK_ListProperties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lists]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lists](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttrId] [int] NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Lists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[Id] [int] NOT NULL,
	[S] [bit] NOT NULL CONSTRAINT [DF_Permissions_S]  DEFAULT ((0)),
	[I] [bit] NOT NULL CONSTRAINT [DF_Permissions_I]  DEFAULT ((0)),
	[U] [bit] NOT NULL CONSTRAINT [DF_Permissions_U]  DEFAULT ((0)),
	[D] [bit] NOT NULL CONSTRAINT [DF_Permissions_D]  DEFAULT ((0)),
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RelatedClient]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelatedClient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientAttrId] [int] NOT NULL,
	[RelatedClientId] [int] NOT NULL,
 CONSTRAINT [PK_RelatedClient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_RelatedClient] UNIQUE NONCLUSTERED 
(
	[ClientAttrId] ASC,
	[RelatedClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StringProperties]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StringProperties](
	[Id] [int] IDENTITY(2,1) NOT NULL,
	[ClientAttrId] [int] NOT NULL,
	[Value] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_StringProperties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Types]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Types] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_ClientAttributes]    Script Date: 31.08.2016 16:42:27 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientAttributes] ON [dbo].[ClientAttributes]
(
	[AttrId] ASC,
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Clients_Firstname]    Script Date: 31.08.2016 16:42:27 ******/
CREATE NONCLUSTERED INDEX [IX_Clients_Firstname] ON [dbo].[Clients]
(
	[Firstname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Clients_INN]    Script Date: 31.08.2016 16:42:27 ******/
CREATE NONCLUSTERED INDEX [IX_Clients_INN] ON [dbo].[Clients]
(
	[INN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Clients_Lastname]    Script Date: 31.08.2016 16:42:27 ******/
CREATE NONCLUSTERED INDEX [IX_Clients_Lastname] ON [dbo].[Clients]
(
	[Lastname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Clients_Middlename]    Script Date: 31.08.2016 16:42:27 ******/
CREATE NONCLUSTERED INDEX [IX_Clients_Middlename] ON [dbo].[Clients]
(
	[Middlename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientAttributes]  WITH CHECK ADD  CONSTRAINT [FK_ClientAttributes_Attributes] FOREIGN KEY([AttrId])
REFERENCES [dbo].[Attributes] ([Id])
GO
ALTER TABLE [dbo].[ClientAttributes] CHECK CONSTRAINT [FK_ClientAttributes_Attributes]
GO
ALTER TABLE [dbo].[ClientAttributes]  WITH CHECK ADD  CONSTRAINT [FK_ClientAttributes_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
GO
ALTER TABLE [dbo].[ClientAttributes] CHECK CONSTRAINT [FK_ClientAttributes_Clients]
GO
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Genders] FOREIGN KEY([GenderId])
REFERENCES [dbo].[Genders] ([Id])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Genders]
GO
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Types] FOREIGN KEY([TypeId])
REFERENCES [dbo].[Types] ([Id])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Types]
GO
ALTER TABLE [dbo].[CreateHistory]  WITH CHECK ADD  CONSTRAINT [FK_CreateHistory_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
GO
ALTER TABLE [dbo].[CreateHistory] CHECK CONSTRAINT [FK_CreateHistory_Clients]
GO
ALTER TABLE [dbo].[ListProperties]  WITH CHECK ADD  CONSTRAINT [FK_ListProperties_ClientAttributes] FOREIGN KEY([ClientAttrId])
REFERENCES [dbo].[ClientAttributes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ListProperties] CHECK CONSTRAINT [FK_ListProperties_ClientAttributes]
GO
ALTER TABLE [dbo].[ListProperties]  WITH CHECK ADD  CONSTRAINT [FK_ListProperties_Lists] FOREIGN KEY([ValueId])
REFERENCES [dbo].[Lists] ([Id])
GO
ALTER TABLE [dbo].[ListProperties] CHECK CONSTRAINT [FK_ListProperties_Lists]
GO
ALTER TABLE [dbo].[Lists]  WITH CHECK ADD  CONSTRAINT [FK_Lists_Attributes] FOREIGN KEY([AttrId])
REFERENCES [dbo].[Attributes] ([Id])
GO
ALTER TABLE [dbo].[Lists] CHECK CONSTRAINT [FK_Lists_Attributes]
GO
ALTER TABLE [dbo].[RelatedClient]  WITH CHECK ADD  CONSTRAINT [FK_RelatedClient_ClientAttributes] FOREIGN KEY([ClientAttrId])
REFERENCES [dbo].[ClientAttributes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RelatedClient] CHECK CONSTRAINT [FK_RelatedClient_ClientAttributes]
GO
ALTER TABLE [dbo].[RelatedClient]  WITH CHECK ADD  CONSTRAINT [FK_RelatedClient_Clients_Related] FOREIGN KEY([RelatedClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RelatedClient] CHECK CONSTRAINT [FK_RelatedClient_Clients_Related]
GO
ALTER TABLE [dbo].[StringProperties]  WITH CHECK ADD  CONSTRAINT [FK_StringProperties_ClientAttributes] FOREIGN KEY([ClientAttrId])
REFERENCES [dbo].[ClientAttributes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StringProperties] CHECK CONSTRAINT [FK_StringProperties_ClientAttributes]
GO
/****** Object:  StoredProcedure [dbo].[Attr_Insert]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE Procedure [dbo].[Attr_Insert]
   @Name  nvarchar(50),
   @TypeId   INT,
   @Id   INT output
   AS 
   INSERT INTO dbo.Attributes ( Name, TypeId ) VALUES (@Name, @TypeId)
   SET @Id = Cast(SCOPE_IDENTITY() as int)

GO
/****** Object:  StoredProcedure [dbo].[Client_Insert]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Client_Insert]
   @INN  NVARCHAR(10),
   @Lastname NVARCHAR(50),
   @Firstname NVARCHAR(50),
   @Middlename NVARCHAR(50),
   @Birthday DATE,
   @GenderId INT,
   @TypeId   INT,
   @Id   INT OUTPUT
   AS 
   INSERT INTO dbo.Clients ( INN, Lastname, Firstname, Middlename, Birthday, GenderId, TypeId ) 
   VALUES (@INN, @Lastname, @Firstname, @Middlename, @Birthday, @GenderId, @TypeId)
   SET @Id = CAST(SCOPE_IDENTITY() AS INT)


GO
/****** Object:  StoredProcedure [dbo].[ListProp_Insert]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListProp_Insert]
   @ClientId INT,
   @AttrId INT,
   @ValueId INT,
   @Id   INT OUTPUT,
   @ClientAttrId   INT OUTPUT
   AS    
   BEGIN TRY
   BEGIN TRAN
	   INSERT INTO dbo.ClientAttributes ( AttrId, ClientId )   VALUES  ( @AttrId, @ClientId)
	   SET @ClientAttrId = CAST(SCOPE_IDENTITY() AS INT)
	   INSERT INTO dbo.ListProperties( ClientAttrId, ValueId ) VALUES (@ClientAttrId, @ValueId)
	   SET @Id = CAST(SCOPE_IDENTITY() AS INT)
    COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH


GO
/****** Object:  StoredProcedure [dbo].[RelatedClient_Insert]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RelatedClient_Insert]
   @ClientId INT,
   @AttrId INT,
   @RelatedClientId INT,
   @Id   INT OUTPUT,
   @ClientAttrId   INT OUTPUT
   AS 
   BEGIN TRY      
	   BEGIN TRAN
	   INSERT INTO dbo.ClientAttributes ( AttrId, ClientId )   VALUES  ( @AttrId, @ClientId)
	   SET @ClientAttrId = CAST(SCOPE_IDENTITY() AS INT)
	   INSERT INTO dbo.RelatedClient( ClientAttrId, RelatedClientId ) VALUES (@ClientAttrId, @RelatedClientId)
	   SET @Id = CAST(SCOPE_IDENTITY() AS INT)
	   COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH



GO
/****** Object:  StoredProcedure [dbo].[StringProp_Insert]    Script Date: 31.08.2016 16:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StringProp_Insert]
   @ClientId int,
   @AttrId INT,
   @Value NVARCHAR(250),
   @Id   INT OUTPUT,
   @ClientAttrId   INT OUTPUT
   AS    
   BEGIN TRY      
	   BEGIN TRAN
   INSERT INTO dbo.ClientAttributes ( AttrId, ClientId )   VALUES  ( @AttrId, @ClientId)
   SET @ClientAttrId = CAST(SCOPE_IDENTITY() AS INT)
   INSERT INTO dbo.StringProperties( ClientAttrId, Value ) VALUES (@ClientAttrId, @Value)
   SET @Id = CAST(SCOPE_IDENTITY() AS INT)
    COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - woman, 1 - man' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clients', @level2type=N'COLUMN',@level2name=N'GenderId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'select' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Permissions', @level2type=N'COLUMN',@level2name=N'S'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'insert' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Permissions', @level2type=N'COLUMN',@level2name=N'I'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'update' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Permissions', @level2type=N'COLUMN',@level2name=N'U'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Permissions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'advanced attributes for client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RelatedClient'
GO
USE [master]
GO
ALTER DATABASE [TestClients2016] SET  READ_WRITE 
GO
