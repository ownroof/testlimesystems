USE [TestClients2016]
GO

/****** Object:  Table [dbo].[Clients]    Script Date: 29.08.2016 15:40:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[INN] [nvarchar](10) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[Firstname] [nvarchar](50) NULL,
	[Middlename] [nvarchar](50) NULL,
	[Birthday] [date] NULL,
	[GenderId] [int] NULL,
	[TypeId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Clients_IsActive]  DEFAULT ((1)),
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Clients] UNIQUE NONCLUSTERED 
(
	[INN] ASC,
	[Lastname] ASC,
	[Firstname] ASC,
	[Middlename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Genders] FOREIGN KEY([GenderId])
REFERENCES [dbo].[Genders] ([Id])
GO

ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Genders]
GO

ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Types] FOREIGN KEY([TypeId])
REFERENCES [dbo].[Types] ([Id])
GO

ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Types]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - woman, 1 - man' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clients', @level2type=N'COLUMN',@level2name=N'GenderId'
GO

