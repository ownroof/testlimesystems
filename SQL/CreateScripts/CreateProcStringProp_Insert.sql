USE [TestClients2016]
GO

/****** Object:  StoredProcedure [dbo].[StringProp_Insert]    Script Date: 31.08.2016 14:51:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[StringProp_Insert]
   @ClientId int,
   @AttrId INT,
   @Value NVARCHAR(250),
   @Id   INT OUTPUT,
   @ClientAttrId   INT OUTPUT
   AS    
   BEGIN TRY      
	   BEGIN TRAN
   INSERT INTO dbo.ClientAttributes ( AttrId, ClientId )   VALUES  ( @AttrId, @ClientId)
   SET @ClientAttrId = CAST(SCOPE_IDENTITY() AS INT)
   INSERT INTO dbo.StringProperties( ClientAttrId, Value ) VALUES (@ClientAttrId, @Value)
   SET @Id = CAST(SCOPE_IDENTITY() AS INT)
    COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH

GO

