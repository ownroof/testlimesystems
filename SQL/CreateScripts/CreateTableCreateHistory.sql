USE [TestClients2016]
GO

/****** Object:  Table [dbo].[CreateHistory]    Script Date: 23.08.2016 0:33:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CreateHistory](
	[Id] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Locale] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CreateHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

