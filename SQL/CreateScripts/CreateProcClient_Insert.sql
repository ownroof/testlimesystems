USE [TestClients2016]
GO

/****** Object:  StoredProcedure [dbo].[Client_Insert]    Script Date: 29.08.2016 15:45:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Client_Insert]
   @INN  NVARCHAR(10),
   @Lastname NVARCHAR(50),
   @Firstname NVARCHAR(50),
   @Middlename NVARCHAR(50),
   @Birthday DATE,
   @GenderId INT,
   @TypeId   INT,
   @Id   INT OUTPUT
   AS 
   INSERT INTO dbo.Clients ( INN, Lastname, Firstname, Middlename, Birthday, GenderId, TypeId ) 
   VALUES (@INN, @Lastname, @Firstname, @Middlename, @Birthday, @GenderId, @TypeId)
   SET @Id = CAST(SCOPE_IDENTITY() AS INT)


GO

