USE [TestClients2016]
GO

/****** Object:  StoredProcedure [dbo].[Attr_Insert]    Script Date: 29.08.2016 15:45:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE Procedure [dbo].[Attr_Insert]
   @Name  nvarchar(50),
   @TypeId   INT,
   @Id   INT output
   AS 
   INSERT INTO dbo.Attributes ( Name, TypeId ) VALUES (@Name, @TypeId)
   SET @Id = Cast(SCOPE_IDENTITY() as int)

GO

