USE [TestClients2016]
GO

/****** Object:  Table [dbo].[ListProperties]    Script Date: 29.08.2016 15:41:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ListProperties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientAttrId] [int] NOT NULL,
	[ValueId] [int] NOT NULL,
 CONSTRAINT [PK_ListProperties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ListProperties]  WITH CHECK ADD  CONSTRAINT [FK_ListProperties_ClientAttributes] FOREIGN KEY([ClientAttrId])
REFERENCES [dbo].[ClientAttributes] ([Id])
GO

ALTER TABLE [dbo].[ListProperties] CHECK CONSTRAINT [FK_ListProperties_ClientAttributes]
GO

ALTER TABLE [dbo].[ListProperties]  WITH CHECK ADD  CONSTRAINT [FK_ListProperties_Lists] FOREIGN KEY([ValueId])
REFERENCES [dbo].[Lists] ([Id])
GO

ALTER TABLE [dbo].[ListProperties] CHECK CONSTRAINT [FK_ListProperties_Lists]
GO

