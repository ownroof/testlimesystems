USE [TestClients2016]
GO

/****** Object:  StoredProcedure [dbo].[ListProp_Insert]    Script Date: 31.08.2016 14:49:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ListProp_Insert]
   @ClientId INT,
   @AttrId INT,
   @ValueId INT,
   @Id   INT OUTPUT,
   @ClientAttrId   INT OUTPUT
   AS    
   BEGIN TRY
   BEGIN TRAN
	   INSERT INTO dbo.ClientAttributes ( AttrId, ClientId )   VALUES  ( @AttrId, @ClientId)
	   SET @ClientAttrId = CAST(SCOPE_IDENTITY() AS INT)
	   INSERT INTO dbo.ListProperties( ClientAttrId, ValueId ) VALUES (@ClientAttrId, @ValueId)
	   SET @Id = CAST(SCOPE_IDENTITY() AS INT)
    COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH


GO

