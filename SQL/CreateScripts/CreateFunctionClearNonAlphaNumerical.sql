USE [TestClients2016]
GO

/****** Object:  UserDefinedFunction [dbo].[ClearNonAlphaNumerical]    Script Date: 23.08.2016 0:36:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ClearNonAlphaNumerical] 
(
	-- Add the parameters for the function here
	@source NVARCHAR(50)
)
RETURNS NVARCHAR(50)
AS
BEGIN
	DECLARE @result NVARCHAR(50) = @source
	DECLARE @pos INT
	SET @pos = PATINDEX('%[^a-zA-Z0-9а-яА-Я ]%',@result)
-- sets @pos to the position of the first character
-- not equal to a letter or a digit (0-9)
 
WHILE @pos > 0 
BEGIN 
  SET @result = STUFF(@result,@pos,1,'')
  -- overwrites the non-alphanumeric character at position @pos with ''
  SET @pos = PATINDEX('%[^a-zA-Z0-9а-яА-Я ]%',@result)
  -- looks for the next non-alphanumeric character
END

	-- Return the result of the function
	RETURN @result

END

GO

