USE [TestClients2016]
GO
/****** Object:  Trigger [dbo].[CheckListMember]    Script Date: 26.08.2016 22:38:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[CheckListMember]  
ON [dbo].[ListProperties] 
FOR INSERT AS
DECLARE @cnt INT  
BEGIN  
	
   SET @cnt = (SELECT COUNT(*) FROM inserted p
INNER JOIN dbo.ClientAttributes ca ON ca.id = p.ClientAttrId
INNER JOIN dbo.Lists l ON (l.AttrId = ca.AttrId AND l.Id = p.ValueId))
    IF @cnt = 0 
		RAISERROR ('Attempt to insert property with alien AttrId is not allowed',16,1)		
	
  --  ELSE    
		--INSERT dbo.ListProperties 
		--SELECT * FROM Inserted   
	
    
END  
