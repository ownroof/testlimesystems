DECLARE @attrId INT = 6;
DECLARE @clientId INT = 0;
DECLARE @i INT
WHILE (@clientId<300000)
	BEGIN
	SET @i = 0
	WHILE (@i<4)
		begin
			BEGIN TRY 
				INSERT INTO dbo.ClientAttributes
						( AttrId, ClientId )
				VALUES  (( ABS(CHECKSUM(NewId())) % 21)+@attrId, @clientId)
			
			END TRY
			BEGIN CATCH
				PRINT N'Unable to insert ClientId = '+CAST(@clientId+1 AS NVARCHAR(50));
			END CATCH
			SET @i = @i +1
		end
	SET @clientId = @clientId + 1
	END