SELECT c1.INN AS RelatedINN, RTRIM(c1.Lastname+' '+c1.Firstname+' '+c1.Middlename) AS RelatedFIO FROM dbo.RelatedClient AS rc 
left JOIN dbo.ClientAttributes AS ca ON ca.Id = rc.ClientAttrId
LEFT JOIN dbo.Clients c1 ON c1.Id = rc.RelatedClientId
WHERE ca.ClientId = -1