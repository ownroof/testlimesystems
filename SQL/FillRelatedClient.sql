DECLARE @i INT = 2;
DECLARE @relatedId INT = (SELECT MAX(id) FROM dbo.RelatedClient);
WHILE (@i<30501)
BEGIN
	BEGIN TRY 
		INSERT INTO dbo.RelatedClient
			( Id, ClientId, RelatedClientId )
		VALUES  ( @relatedId, -- Id - int
			  @i+1, -- ClientId - int
			  @i  -- RelatedClientId - int
			  );
	END TRY
	BEGIN CATCH
		PRINT N'Unable to insert ClientId = '+CAST(@i+1 AS NVARCHAR(50))+' and RelatedClientId = '+CAST(@i AS NVARCHAR(50));
	END CATCH
SET @i = @i+1
SET @relatedId = @relatedId + 1
END
