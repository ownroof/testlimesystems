﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLToolkit.Reflection;
using WebApplication1.Models;
using WebApplication1.Views;
using WebApplication1.Presenters;

namespace WebApplication1
{
    public partial class SearchForm : Page, ISearchClientView<Client>
    {
        protected ISearchClientPresenter presenter;
        public IEnumerable<Client> DataSource { get; set; }
        public bool IsByInn { get; set; }
        public bool IsByFIO { get; set; }
        public bool IsByRelatedClient { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindBtnChecked();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            presenter = new SearchClientPresenter();
            presenter.Initialize(this);
            presenter.SearchClients(this.textForSearch.Text);
        }

        public void BindData()
        {
            gridClients.DataSource = DataSource;
            gridClients.DataBind();
        }       

        protected void validatorForSearch_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = args.Value.IsAlphaNumerical();
        }

        protected void BindBtnChecked(object source, EventArgs e)
        {
            BindBtnChecked();
        }

        protected void BindBtnChecked()
        {
            IsByInn = btnINN.Checked;
            IsByFIO = btnFIO.Checked;
            IsByRelatedClient = btnRelated.Checked;
        }

        protected string GetUrlToEdit(int Id)
        {
            return string.Format("~/Forms/ClientEditForm?id={0}", Id);
        }


    }
}