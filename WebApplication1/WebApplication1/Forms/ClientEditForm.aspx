﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientEditForm.aspx.cs" Inherits="WebApplication1.Forms.ClientEditForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">    
    <telerik:RadButton runat="server" ID="btnSave" Text="Сохранить" ClientIDMode="Static" OnClick="btnSave_Click"></telerik:RadButton>
    <asp:Panel runat="server"  ID="ConstAttributes" ClientIDMode="Static" Width="100%" >    
        <div class="row"><div class="col-md-12">Основные реквизиты</div></div>
        <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblINN" runat="server" Text="ИНН"></telerik:RadLabel>
            </div>
            <div class="col-md-10">
                <telerik:RadTextBox runat="server" ID="tbINN" Text="<%# ClientConstAttrs.INN %>" MaxLength="10"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblLastName" runat="server" Text="Фамилия"></telerik:RadLabel>
            </div>
            <div class="col-md-10" >
                <telerik:RadTextBox runat="server" ID="tbLastName" Text="<%# ClientConstAttrs.Lastname %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblFirstName" runat="server" Text="Имя"></telerik:RadLabel>
            </div>
            <div class="col-md-10">
                <telerik:RadTextBox runat="server" ID="tbFirstName" Text="<%# ClientConstAttrs.Firstname %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblMiddleName" runat="server" Text="Отчество"></telerik:RadLabel>
            </div>
            <div class="col-md-10">
                <telerik:RadTextBox runat="server" ID="tbMiddleName" Text="<%# ClientConstAttrs.Middlename %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblBirthday" runat="server" Text="Дата рождения"></telerik:RadLabel>
            </div>
            <div class="col-md-10">                
                <telerik:RadDateInput runat="server" ID="dateBirthday" SelectedDate=<%# ClientConstAttrs.Birthday %>/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblGender" runat="server" Text="Пол"></telerik:RadLabel>
            </div>
            <div class="col-md-10">
                <telerik:RadDropDownList runat="server" ID="ddlGender" EnableVirtualScrolling="false" 
                    DataTextField="Name" DataValueField="Id" />
            </div>
        </div>
               <div class="row">
            <div class="col-md-2">
                <telerik:RadLabel ID="lblType" runat="server" Text="Тип клиента"></telerik:RadLabel>
            </div>
            <div class="col-md-10">                                
                <telerik:RadDropDownList runat="server" ID="ddlType" EnableVirtualScrolling="false" 
                    DataTextField="Name" DataValueField="Id" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="VarAttributes" ClientIDMode="Static">
    <div class="row"><div class="col-md-12">Дополнительные реквизиты</div></div>
    <div class="row"><div class="col-md-12">Строки</div></div>
    <div class="row">
        <telerik:RadGrid ID="gridStringProperties" runat="server" AutoGenerateColumns="false" AllowPaging="True" Culture="ru-RU" 
            RenderMode="Lightweight" OnItemDataBound="gridStringProperties_ItemDataBound" 
            OnInsertCommand="gridStringProperties_InsertCommand" OnDeleteCommand="gridStringProperties_DeleteCommand" 
            OnUpdateCommand="gridStringProperties_UpdateCommand" OnNeedDataSource="gridStringProperties_NeedDataSource">
            <MasterTableView DataKeyNames="Id, AttrId, AttrName" Width="100%" CommandItemDisplay="Top">
                <CommandItemSettings AddNewRecordText="Добавить реквизит" />
                <Columns>                    
                    <telerik:GridEditCommandColumn EditText="Редактировать" UpdateText="Сохранить" CancelText="Отмена" />                
                    <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" Visible="false" Readonly="true"/>
                    <telerik:GridBoundColumn UniqueName="ClientAttrId" HeaderText="ClientAttrId" DataField="ClientAttrId"  Visible="false" Readonly="true"/>                    
                    <telerik:GridTemplateColumn  UniqueName="AttrId" HeaderText="Реквизит" DataField="AttrId">
                        <ItemTemplate>
                            <%# Eval("AttrName") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# Eval("AttrName") %>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="ddlAttr" DataValueField="Id" DataTextField="Name"/>                        
                        </InsertItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn UniqueName="Value" HeaderText="Значение" DataField="Value"/>                                        
                    <telerik:GridButtonColumn Text="Удалить" CommandName="Delete" ButtonType="LinkButton" />                                              
                </Columns>                                
            </MasterTableView> 
        </telerik:RadGrid>
    </div>
    <div class="row"><div class="col-md-12">Справочники</div></div>
    <div class="row">
        <telerik:RadGrid ID="gridListProperties" runat="server" AutoGenerateColumns="false" AllowPaging="True" Culture="ru-RU" 
            RenderMode="Lightweight" OnItemDataBound="gridListProperties_ItemDataBound"
            OnInsertCommand="gridListProperties_InsertCommand" OnDeleteCommand="gridListProperties_DeleteCommand"
            OnUpdateCommand="gridListProperties_UpdateCommand" OnNeedDataSource="gridListProperties_NeedDataSource">
            <MasterTableView DataKeyNames="Id, AttrId, AttrName, ValueId" Width="100%" CommandItemDisplay="Top">
                <CommandItemSettings AddNewRecordText="Добавить реквизит" />
                <Columns>                    
                    <telerik:GridEditCommandColumn EditText="Редактировать" UpdateText="Сохранить" CancelText="Отмена" />                
                    <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" Visible="false" Readonly="true"/>
                    <telerik:GridBoundColumn UniqueName="ClientAttrId" HeaderText="ClientAttrId" DataField="ClientAttrId"  Visible="false" Readonly="true"/>                    
                    <telerik:GridTemplateColumn  UniqueName="AttrId" HeaderText="Реквизит" DataField="AttrId">
                        <ItemTemplate>
                            <%# Eval("AttrName") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# Eval("AttrName") %>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <telerik:RadDropDownList OnSelectedIndexChanged="ddlAttr_SelectedIndexChanged" AutoPostBack="true"                                  
                                RenderMode="Lightweight" runat="server" ID="ddlAttr" DataValueField="Id" DataTextField="Name"/>                        
                        </InsertItemTemplate>
                    </telerik:GridTemplateColumn>                    
                    <telerik:GridTemplateColumn UniqueName="ValueId" HeaderText="Значение" DataField="ValueId">
                        <ItemTemplate>
                            <%# Eval("Value") %>
                        </ItemTemplate>
                        <EditItemTemplate>       
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">                    
                            <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="ddlListProp" AllowCustomText="true" 
                                Filter="StartsWith" OnItemsRequested="ddlListProp_ItemsRequested" 
                                 DataValueField="Id" DataTextField="Value" EnableLoadOnDemand="true" DropDownHeight="200px"/>
                            </telerik:RadAjaxPanel>                        
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn Text="Удалить" CommandName="Delete" ButtonType="LinkButton" />                                              
                </Columns>                                
            </MasterTableView>            
        </telerik:RadGrid>
    </div>
        <div class="row"><div class="col-md-12">Справочники</div></div>
    <div class="row">
        <telerik:RadGrid ID="gridRelatedClient" runat="server" AutoGenerateColumns="false" AllowPaging="True" Culture="ru-RU" 
            RenderMode="Lightweight" OnItemDataBound="gridRelatedClient_ItemDataBound"
            OnInsertCommand="gridRelatedClient_InsertCommand" OnDeleteCommand="gridRelatedClient_DeleteCommand"
            OnUpdateCommand="gridRelatedClient_UpdateCommand" OnNeedDataSource="gridRelatedClient_NeedDataSource">
            <MasterTableView DataKeyNames="Id, AttrId, AttrName, RelatedClientId" Width="100%" CommandItemDisplay="Top">
                <CommandItemSettings AddNewRecordText="Добавить реквизит" />                
                <Columns>                    
                    <telerik:GridEditCommandColumn EditText="Редактировать" UpdateText="Сохранить" CancelText="Отмена" />                
                    <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" Visible="false" Readonly="true"/>
                    <telerik:GridBoundColumn UniqueName="ClientAttrId" HeaderText="ClientAttrId" DataField="ClientAttrId"  Visible="false" Readonly="true"/>                    
                    <telerik:GridTemplateColumn  UniqueName="AttrId" HeaderText="Реквизит" DataField="AttrId">
                        <ItemTemplate>
                            <%# Eval("AttrName") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# Eval("AttrName") %>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="ddlAttr" DataValueField="Id" DataTextField="Name"/>                        
                        </InsertItemTemplate>
                    </telerik:GridTemplateColumn>                    
                    <telerik:GridTemplateColumn UniqueName="RelatedClientId" HeaderText="Значение" DataField="RelatedClientId">
                        <ItemTemplate>
                            <%# Eval("RelatedClientName") %>
                        </ItemTemplate>
                        <EditItemTemplate>                              
                            <telerik:RadAjaxPanel ID="panelSearch" runat="server">
                            <telerik:RadTextBox runat="server" ID="tbSearch"/>
                                <telerik:RadButton runat="server" ID="btnSearch" OnClick="btnSearch_Click" AutoPostBack="true"/>
                            <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="ddlRelatedClient"  
                                DataValueField="Id" DataTextField="Name" EnableVirtualScrolling="true" DropDownHeight="200px"/>   
                                     </telerik:RadAjaxPanel>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn Text="Удалить" CommandName="Delete" ButtonType="LinkButton" />                                              
                </Columns>                                
            </MasterTableView> 
        </telerik:RadGrid>
    </div>
    </asp:Panel>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">  
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="gridStringProperties">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="gridStringProperties" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="gridListProperties">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="gridListProperties" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="gridRelatedClient">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="gridRelatedClient" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">    
    </script>
</telerik:RadCodeBlock>
</asp:Content>
