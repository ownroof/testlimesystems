﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using WebApplication1.Models;
using WebApplication1.Presenters;
using WebApplication1.Views;

namespace WebApplication1.Forms
{
    public partial class ClientEditForm : Page, IClientFormView
    {
        public Client ClientConstAttrs { get; set; }
        protected IClientFormPresenter clientPresenter;

        protected IGendersPresenter gendersPresenter;
        protected IClientTypesPresenter clientTypesPresenter;
        protected IClientPropsBasePresenter<ClientStringProperty> stringPropsPresenter;
        protected IClientPropsBasePresenter<ClientListProperty> listPropsPresenter;
        protected IClientPropsBasePresenter<ClientRelatedClient> relatedClientPresenter;

        #region IClientFormView
        public void BindData()
        {
            Page.DataBind();
        }

        public void SaveClientProps(int clientId)
        {
            SaveStringProps(clientId);
            SaveListProps(clientId);
            SaveRelatedClients(clientId);            
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            FillClients();
            FillGenders();
            FillClientTypes();
        }
        #region init main attributes ddl
        protected void FillClients()
        {
            clientPresenter = new ClientFormPresenter();
            clientPresenter.Initialize(this);           
            clientPresenter.LoadClientMainAttrs(GetClientId());
        }

        protected void FillGenders()
        {
            gendersPresenter = new GendersPresenter();
            gendersPresenter.Initialize(new DropdownView<Gender>() { Dropdown = this.ddlGender, SelectedId = (ClientConstAttrs == null) ? null : ClientConstAttrs.GenderId });
            gendersPresenter.LoadAllGenders();
        }

        protected void FillClientTypes()
        {
            clientTypesPresenter = new ClientTypesPresenter();
            clientTypesPresenter.Initialize(new DropdownView<ClientType>() { Dropdown = this.ddlType, SelectedId = (ClientConstAttrs == null) ? null : ClientConstAttrs.TypeId });
            clientTypesPresenter.LoadAllClientTypes();
        }
        #endregion

        #region init presenters
        protected void InitStringPropsPresenter(INonSelectableDSView<ClientStringProperty> gridSource)
        {
            stringPropsPresenter = new StringPropertiesPresenter();
            stringPropsPresenter.Initialize(gridSource, ViewState["StringProps"]);            
        }

        protected void InitListPropsPresenter(INonSelectableDSView<ClientListProperty> gridSource)
        {
            listPropsPresenter = new ListPropertiesGridPresenter();
            listPropsPresenter.Initialize(gridSource, ViewState["ListProps"]);            
        }

        protected void InitRelatedClientPropsPresenter(INonSelectableDSView<ClientRelatedClient> gridSource)
        {
            relatedClientPresenter = new RelatedClientPresenter();
            relatedClientPresenter.Initialize(gridSource, ViewState["RelatedClient"]);
        }
        #endregion

        

        #region save methods
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var client = new Client()
            {
                INN = tbINN.Text,
                Lastname = tbLastName.Text,
                Firstname = tbFirstName.Text,
                Middlename = tbMiddleName.Text,
                Birthday = dateBirthday.SelectedDate,
                GenderId = int.Parse(ddlGender.SelectedValue),
                TypeId = int.Parse(ddlType.SelectedValue)
            };

            clientPresenter = new ClientFormPresenter();
            clientPresenter.Initialize(this);
            var clientId = GetClientId();
            if (clientId.HasValue)
                client.Id = clientId.Value;
            var itemHistory = new ClientCreateHistory()
            {
                Login = User.Identity.Name,
                Locale = Context.Request.UserLanguages[0],
                Date = DateTime.Now,
            };

            clientPresenter.SaveClientWithProps(client, itemHistory);
            Response.Redirect(string.Format("~/Forms/ClientEditForm?id={0}", client.Id));
        }
      
        protected void SaveStringProps(int clientId)
        {
            InitStringPropsPresenter(new GridView<ClientStringProperty>() { Grid = this.gridStringProperties });
            stringPropsPresenter.SaveProperties(clientId);
        }

        protected void SaveListProps(int clientId)
        {
            InitListPropsPresenter(new GridView<ClientListProperty>() { Grid = this.gridListProperties });
            listPropsPresenter.SaveProperties(clientId);
        }

        protected void SaveRelatedClients(int clientId)
        {
            InitRelatedClientPropsPresenter(new GridView<ClientRelatedClient>() { Grid = this.gridRelatedClient });
            relatedClientPresenter.SaveProperties(clientId);
        }

        

        #endregion


        #region gridStringProperties events
        protected void gridStringProperties_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridEditableItem && e.Item.IsInEditMode && e.Item is GridEditFormInsertItem)) return;

            InitAttrList(e.Item, AttrTypes.String);
        }

        protected void gridStringProperties_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var attr = GetSelectedAttr(parentControl: item);
            if (attr == null)
            {
                e.Canceled = true;
                return;
            }

            var values = GetEditingControlValues(item);

            var prop = new ClientStringProperty() {
                AttrId = attr.Id,
                AttrName = attr.Name,
                Value = (string)values["Value"] };

            var gridSource = new GridView<ClientStringProperty>() { Grid = sender as RadGrid };
            InitStringPropsPresenter(gridSource);
            stringPropsPresenter.Create(prop);
            ViewState["StringProps"] = gridSource.DataSource;
            AddUsedAttr(attr);
        }

        protected void gridStringProperties_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;
            
            var gridSource = new GridView<ClientStringProperty>() { Grid = sender as RadGrid };
            InitStringPropsPresenter(gridSource);
            stringPropsPresenter.Delete(new ClientStringProperty() { Id = GetKeyId(item) });
            ViewState["StringProps"] = gridSource.DataSource;           
            DeleteUsedAttr(new Models.Attribute() { Id = GetAttrId(item), Name = GetAttrName(item) });
        }

        protected void gridStringProperties_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem; 
            if (item == null) return;
            var values = GetEditingControlValues(item);

            var gridSource = new GridView<ClientStringProperty>() { Grid = sender as RadGrid };
            InitStringPropsPresenter(gridSource);
            stringPropsPresenter.Update(new ClientStringProperty() {
                Id = GetKeyId(item),
                AttrId = GetAttrId(item),                
                Value = (string)values["Value"] });
            ViewState["StringProps"] = gridSource.DataSource;

        }

        protected void gridStringProperties_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {            
            var gridSource = new GridView<ClientStringProperty>() { Grid = sender as RadGrid };
            InitStringPropsPresenter(gridSource);
            stringPropsPresenter.LoadProperties(GetClientId());
            ViewState["StringProps"] = gridSource.DataSource;
        }

        #endregion

        #region gridListProperties events
        protected void gridListProperties_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridEditableItem && e.Item.IsInEditMode)) return;
            var selAttrId = InitChangeableAttrList(e.Item, AttrTypes.List);
            InitPropList(e.Item, selAttrId);
            ViewState["AttrId"] = selAttrId;
        }

        protected void gridListProperties_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var attr = GetSelectedAttr(parentControl: item);
            var propVal = GetSelectedComboItem(parentControl: item, ddlName: "ddlListProp");
            if (propVal == null || attr == null)
            {
                e.Canceled = true;
                return;
            }

            var prop = new ClientListProperty()
            {
                AttrId = attr.Id,
                AttrName = attr.Name,
                ValueId = Convert.ToInt32(propVal.Value),
                Value = propVal.Text
            };

            var gridSource = new GridView<ClientListProperty>() { Grid = sender as RadGrid };
            InitListPropsPresenter(gridSource);
            listPropsPresenter.Create(prop);
            ViewState["ListProps"] = gridSource.DataSource;
            AddUsedAttr(attr);
        }

        protected void gridListProperties_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var gridSource = new GridView<ClientListProperty>() { Grid = sender as RadGrid };
            InitListPropsPresenter(gridSource);
            listPropsPresenter.Delete(new ClientListProperty() { Id = GetKeyId(item) });
            ViewState["ListProps"] = gridSource.DataSource;
            DeleteUsedAttr(new Models.Attribute() { Id = GetAttrId(item), Name = GetAttrName(item) });
        }

        protected void gridListProperties_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;
            var propVal = GetSelectedComboItem(parentControl: item, ddlName: "ddlListProp");
            if (propVal.Value == null)
            {
                e.Canceled = true;
                return;
            }

            var gridSource = new GridView<ClientListProperty>() { Grid = sender as RadGrid };
            InitListPropsPresenter(gridSource);
            listPropsPresenter.Update(new ClientListProperty()
            {
                Id = GetKeyId(item),
                AttrId = GetAttrId(item),
                ValueId = Convert.ToInt32(propVal.Value),
                Value = propVal.Text
            });
            ViewState["ListProps"] = gridSource.DataSource;
        }

        protected void gridListProperties_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var gridSource = new GridView<ClientListProperty>() { Grid = sender as RadGrid };
            InitListPropsPresenter(gridSource);
            listPropsPresenter.LoadProperties(GetClientId());
            ViewState["ListProps"] = gridSource.DataSource;
        }

        protected void ddlListProp_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
      {
            var combo = sender as RadComboBox;
            var gridItem = combo.NamingContainer as GridItem;
            var selPropId = GetEditingPropValueId(gridItem);
            var attrId = (int?)ViewState["AttrId"];

            var ddlSource = new ComboboxView<ClientListProperty>() { Dropdown = sender as RadComboBox };
            var propPresenter = new ListPropertiesListPresenter();
            propPresenter.Initialize(ddlSource);
            propPresenter.LoadPropertiesByAttrId(attrId, selPropId, e.Text);
        }

        #endregion


        #region ddlAttr events
        protected void ddlAttr_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {
            var ddlAttr = sender as RadDropDownList;
            InitPropList(ddlAttr.NamingContainer as GridItem, Convert.ToInt32(e.Value));
            ViewState["AttrId"] = ddlAttr.SelectedValue;
        }
        #endregion

        #region gridRelatedClient events

        protected void gridRelatedClient_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!(e.Item is GridEditableItem && e.Item.IsInEditMode)) return;
            var selAttrId = InitChangeableAttrList(e.Item, AttrTypes.RelatedClient);            
        }

        protected void gridRelatedClient_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var attr = GetSelectedAttr(parentControl: item);            
            var selClient = this.GetSelectedClientId(parentControl: item);
            if (selClient == null || attr == null)
            {
                e.Canceled = true;
                return;
            }

            var prop = new ClientRelatedClient()
            {
                AttrId = attr.Id,
                AttrName = attr.Name,
                RelatedClientId = selClient.Id,
                RelatedClientName = selClient.Name
            };

            var gridSource = new GridView<ClientRelatedClient>() { Grid = sender as RadGrid };
            InitRelatedClientPropsPresenter(gridSource);
            relatedClientPresenter.Create(prop);
            ViewState["RelatedClient"] = gridSource.DataSource;
            AddUsedAttr(attr);
        }

        protected void gridRelatedClient_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var gridSource = new GridView<ClientRelatedClient>() { Grid = sender as RadGrid };
            InitRelatedClientPropsPresenter(gridSource);
            relatedClientPresenter.Delete(new ClientRelatedClient() { Id = GetKeyId(item) });
            ViewState["RelatedClient"] = gridSource.DataSource;
            DeleteUsedAttr(new Models.Attribute() { Id = GetAttrId(item), Name = GetAttrName(item) });
        }

        protected void gridRelatedClient_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;
            var selClient = this.GetSelectedClientId(parentControl: item);
            if (selClient == null)
            {
                e.Canceled = true;
                return;
            }

            var gridSource = new GridView<ClientRelatedClient>() { Grid = sender as RadGrid };
            InitRelatedClientPropsPresenter(gridSource);
            relatedClientPresenter.Update(new ClientRelatedClient()
            {
                Id = GetKeyId(item),
                AttrId = GetAttrId(item),
                RelatedClientId = selClient.Id,
                RelatedClientName = selClient.Name
            });
            ViewState["RelatedClient"] = gridSource.DataSource;
        }

        protected void gridRelatedClient_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var gridSource = new GridView<ClientRelatedClient>() { Grid = sender as RadGrid };
            InitRelatedClientPropsPresenter(gridSource);
            relatedClientPresenter.LoadProperties(GetClientId());
            ViewState["RelatedClient"] = gridSource.DataSource;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var btn = sender as RadButton;
            var tbSearch = btn.Parent.FindControl("tbSearch") as RadTextBox;
            var ddlRelatedClient = btn.Parent.FindControl("ddlRelatedClient") as RadDropDownList;

            var presenter = new SearchClientShortPresenter();
            presenter.Initialize(new SearchClientView<RadDropDownList, ShortClient>() { searchElement = ddlRelatedClient });
            presenter.SearchClientsLike(tbSearch.Text);
        }
        #endregion

        #region helpers

        protected int? GetClientId()
        {
            if (!Request.QueryString.HasKeys() || Request.QueryString["id"] == null)
                return null;
            return int.Parse(Request.QueryString["id"]);
        }

        protected int? InitAttrList(GridItem item, AttrTypes type)
        {
            var ddl = item.FindControl("ddlAttr") as RadDropDownList;
            var ddlSource = new DropdownView<Models.Attribute>() { Dropdown = ddl };

            var attrPresenter = new AttributesListPresenter();
            var usedAttrs = (IEnumerable<Models.Attribute>)ViewState["UsedAttrs"];
            attrPresenter.Initialize(ddlSource, usedAttrs);
            attrPresenter.LoadAttributesByType((int)type, GetClientId());
            return ddlSource.SelectedId;
        }

        protected int? InitChangeableAttrList(GridItem item, AttrTypes type)
        {
            int? selAttrId = null;
            if (item as GridEditFormInsertItem == null)
                selAttrId = GetAttrId(item as GridEditableItem);
            else
                selAttrId = InitAttrList(item, type);
            return selAttrId;
        }

        protected void InitPropList(GridItem item, int? attrId)
        {
            var propPresenter = InitComboPropList(item);
            var selPropId = GetEditingPropValueId(item);

            propPresenter.LoadPropertiesByAttrId(attrId, selPropId);
        }



        protected IListPropertiesListPresenter InitComboPropList(GridItem item)
        {
            var ddl = item.FindControl("ddlListProp") as RadComboBox;
            var ddlSource = new ComboboxView<ClientListProperty>() { Dropdown = ddl };
            var propPresenter = new ListPropertiesListPresenter();
            propPresenter.Initialize(ddlSource);
            return propPresenter;
        }

        protected int? GetEditingPropValueId(GridItem item)
        {
            int? selPropId = null;
            if (item as GridEditFormInsertItem == null)
                selPropId = GetValueId(item as GridEditableItem);
            return selPropId;
        }

        protected int? GetEditingAttrId(GridItem item)
        {
            int? selAttrId = null;
            if (item as GridEditFormInsertItem == null)
                selAttrId = GetAttrId(item as GridEditableItem);
            return selAttrId;
        }

        protected Models.Attribute GetSelectedAttr(Control parentControl)
        {
            var selected = GetSelectedDDLtem(parentControl, "ddlAttr");
            if (selected == null) return null;
            return new Models.Attribute()
            {
                Id = int.Parse(selected.Value),
                Name = (string)selected.Text
            };
        }

        protected DropDownListItem GetSelectedDDLtem(Control parentControl, string ddlName)
        {
            var ddl = parentControl.FindControl(ddlName) as RadDropDownList;
            if (ddl == null) return null;
            if (ddl.Items.Count == 0) return null;

            return ddl.SelectedItem;
        }

        protected RadComboBoxItem GetSelectedComboItem(Control parentControl, string ddlName)
        {
            var ddl = parentControl.FindControl(ddlName) as RadComboBox;
            if (ddl == null) return null;
            if (ddl.Items.Count == 0) return null;

            return ddl.SelectedItem;
        }

        protected ShortClient GetSelectedClientId(Control parentControl)
        {
            var selected = GetSelectedDDLtem(parentControl, "ddlRelatedClient");
            if (selected == null) return null;
            return new ShortClient()
            {
                Id = int.Parse(selected.Value),
                Name = (string)selected.Text
            };
        }

        protected IDictionary GetEditingControlValues(GridEditableItem item)
        {
            var values = new Hashtable();
            item.ExtractValues(values);
            return values;
        }

        protected int GetKeyId(GridEditableItem item)
        {
            return Convert.ToInt32(item.GetDataKeyValue("Id"));
        }

        protected int GetAttrId(GridEditableItem item)
        {
            return Convert.ToInt32(item.GetDataKeyValue("AttrId"));
        }

        protected string GetAttrName(GridEditableItem item)
        {
            return item.GetDataKeyValue("AttrName").ToString();
        }

        protected int GetValueId(GridEditableItem item)
        {
            return Convert.ToInt32(item.GetDataKeyValue("ValueId"));
        }

        protected void AddUsedAttr(Models.Attribute attr)
        {
            var attrs = (List<Models.Attribute>)ViewState["UsedAttrs"];
            if (attrs == null)
                attrs = new List<Models.Attribute>();
            attrs.Add(attr);
            ViewState["UsedAttrs"] = attrs;
        }

        protected void DeleteUsedAttr(Models.Attribute attr)
        {
            var attrs = (List<Models.Attribute>)ViewState["UsedAttrs"];
            if (attrs == null) return;
            attrs.RemoveAll(a => a.Id == attr.Id && a.Name == attr.Name);
            ViewState["UsedAttrs"] = attrs;
        }

        #endregion

 
    }
}