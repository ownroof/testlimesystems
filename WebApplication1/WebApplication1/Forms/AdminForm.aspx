﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminForm.aspx.cs" Inherits="WebApplication1.AdminForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadGrid ID="gridAttr" runat="server" Culture="ru-RU" AutoGenerateColumns="False" ClientIDMode="Static" 
        OnInsertCommand="gridAttr_InsertCommand" PageSize="20" OnItemDataBound="gridAttr_ItemDataBound" 
        OnNeedDataSource="gridAttr_NeedDataSource" OnDeleteCommand="gridAttr_DeleteCommand" AllowPaging="True"
         OnItemEvent="gridAttr_ItemEvent" OnUpdateCommand="gridAttr_UpdateCommand" >  
<GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>

        <SortingSettings SortedAscToolTip="Сортировать по возрастанию" SortedDescToolTip="Сортировать по убыванию" SortToolTip="Нажмите тут, чтобы отсортировать" />
        
        <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top">
            <CommandItemSettings AddNewRecordText="Добавить атрибут" />
            <RowIndicatorColumn Visible="False">
            </RowIndicatorColumn>
            <Columns>
                <telerik:GridEditCommandColumn EditText="Редактировать" InsertText="Добавить атрибут" UpdateText="Сохранить" CancelText="Отмена" />
                <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" SortExpression="Id" Visible="false" Readonly="true"/>
                <telerik:GridBoundColumn UniqueName="Name" HeaderText="Свойство" DataField="Name" SortExpression="Name"/>
                <telerik:GridTemplateColumn  UniqueName="TypeId" HeaderText="Тип" DataField="TypeId" SortExpression="Type">
                    <ItemTemplate>
                            <%# Eval("Type") %>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="ddlType" DataValueField="Id"
                                DataTextField="Name" OnClientSelectedIndexChanged="OnSelectedIndexChanged_Insert">
                        </telerik:RadDropDownList>
                        <div id="ListContainerInsert" style="display: none">
                        Справочник:
                        <telerik:RadTextBox TextMode="MultiLine" ClientIDMode="Static" RenderMode="Lightweight" Height="200px" 
                            ID="tbListValues" runat="server" Width="600px"></telerik:RadTextBox>
                        </div>
                        <%--<div id="RelatedContainerInsert" style="display: none">
                            Клиент:
                            <telerik:RadDropDownList ID="ddlClient" runat="server" RenderMode="Lightweight" DataValueField="Id"
                                 DataTextField="ClientName" Width="300px" DefaultMessage="Введите ФИО:"
                                DropDownHeight="200px" EnableVirtualScrolling="true" ClientIDMode="static">
                            </telerik:RadDropDownList>
                        </div>--%>
                    </InsertItemTemplate>
                    <EditItemTemplate>                        
                        <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="ddlType" DataValueField="Id" Enabled="false"
                                DataTextField="Name" OnClientSelectedIndexChanged="OnSelectedIndexChanged_Edit" OnClientLoad="OnSelectedIndexChanged_Edit">
                        </telerik:RadDropDownList>
                        
                        <div id="ListContainerEdit" style="display: inline">
                        Справочник:
                        <telerik:RadGrid ID="gridLists" runat="server" RenderMode="Lightweight" AutoGenerateColumns="False" 
                                ClientIDMode="Static" OnInsertCommand="gridLists_InsertCommand" OnDeleteCommand="gridLists_DeleteCommand"
                             OnNeedDataSource="gridLists_NeedDataSource"  AllowPaging="True" Culture="ru-RU" PageSize="5"
                            Style="width: auto;">                                                                              
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>                            
                            <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top" Width="100%">
                                <Columns>                                    
                                    <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" Visible="false" Readonly="true"/>
                                    <telerik:GridBoundColumn UniqueName="Value" HeaderText="Элемент списка" DataField="Value"/>
                                    <telerik:GridButtonColumn Text="Удалить" CommandName="Delete" Visible="true" ButtonType="LinkButton" />
                                </Columns>                                
                            </MasterTableView>                                                        
                        </telerik:RadGrid>
                        </div>
                    </EditItemTemplate>
                </telerik:GridTemplateColumn>


                <telerik:GridButtonColumn Text="Удалить" CommandName="Delete" />
            </Columns>

<EditFormSettings>
<EditColumn UniqueName="EditCommandColumn1" FilterControlAltText="Filter EditCommandColumn1 column"></EditColumn>
</EditFormSettings>
        </MasterTableView>
    </telerik:RadGrid>
        <script type="text/javascript">
    function OnSelectedIndexChanged_Insert(sender, eventArgs) {
        OnSelectedIndexChanged(sender, eventArgs, "ListContainerInsert");                   
    }
    function OnSelectedIndexChanged_Edit(sender, eventArgs) {
        OnSelectedIndexChanged(sender, eventArgs, "ListContainerEdit")
    }
    function OnSelectedIndexChanged(sender, eventArgs, containerName) {
        var attrType = sender.get_selectedItem().get_value();
        var listContainer = document.getElementById(containerName);
        listContainer.style = "display: none";
        if (attrType == 1) //list
            listContainer.style = "display: inline";
    }    

</script>
</asp:Content>
