﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchForm.aspx.cs" Inherits="WebApplication1.SearchForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h4>Поиск клиента</h4>
        </div>
    </div>
    <div class="row">
        <telerik:RadButton ID="btnINN" runat="server" RenderMode="Lightweight" ToggleType="Radio" EnableViewState="true" 
            Text="по ИНН" AutoPostBack="false" GroupName="SearchType" ButtonType="ToggleButton" Checked="true" OnCheckedChanged="BindBtnChecked"></telerik:RadButton>
        <telerik:RadButton ID="btnFIO" runat="server" RenderMode="Lightweight" ToggleType="Radio"  EnableViewState="true"
            Text="по ФИО" AutoPostBack="false" GroupName="SearchType" ButtonType="ToggleButton" OnCheckedChanged="BindBtnChecked"></telerik:RadButton>
        <telerik:RadButton ID="btnRelated" runat="server" RenderMode="Lightweight" ToggleType="Radio" EnableViewState="true" 
            Text="по ФИО связанного клиента" AutoPostBack="false" GroupName="SearchType" ButtonType="ToggleButton" OnCheckedChanged="BindBtnChecked"></telerik:RadButton>
    </div>
    <div class="row">
        <div class="col-md-12">
            <telerik:RadTextBox ID="textForSearch" Runat="server" LabelWidth="64px" Resize="None" Width="400px" 
                EmptyMessage="ИНН/ФИО/ФИО связанного клиента" RenderMode="Lightweight">                
            </telerik:RadTextBox>
            <telerik:RadButton ID="btnSearch" runat="server" Text="Найти" OnClick="btnSearch_Click" RenderMode="Lightweight">
            </telerik:RadButton>
            <asp:HyperLink ID="lnkCreateClient" runat="server" Text="Создать" NavigateUrl="~/Forms/ClientEditForm">
            </asp:HyperLink>
            <asp:CustomValidator ID="validatorForSearch" runat="server" ControlToValidate="textForSearch" ErrorMessage="Допустимы только буквы и цифры!" OnServerValidate="validatorForSearch_ServerValidate"></asp:CustomValidator>
            </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <telerik:RadGrid ID="gridClients" runat="server" AutoGenerateColumns="false" AllowPaging="True" Culture="ru-RU" 
                RenderMode="Lightweight">
                <MasterTableView DataKeyNames="Id" Width="100%">
                            <Columns>                                    
                                <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" Visible="false" Readonly="true"/>
                                <telerik:GridBoundColumn UniqueName="INN" HeaderText="ИНН" DataField="INN"/>
                                <telerik:GridBoundColumn UniqueName="LastName" HeaderText="Фамилия" DataField="Lastname"/>
                                <telerik:GridBoundColumn UniqueName="Firstname" HeaderText="Имя" DataField="Firstname"/>
                                <telerik:GridBoundColumn UniqueName="Middlename" HeaderText="Отчество" DataField="Middlename"/>
                                <telerik:GridBoundColumn UniqueName="IsActive" HeaderText="Активный" DataField="IsActive"/>
                                <telerik:GridHyperLinkColumn Text="Редактирование" DataNavigateUrlFields="Id"
                                    DataNavigateUrlFormatString="~/Forms/ClientEditForm?id={0}"/>      
                                                                  
                            </Columns>                                
                        </MasterTableView> 
            </telerik:RadGrid>

        </div>
    </div>
</asp:Content>
