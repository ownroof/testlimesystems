﻿using System.Collections.Generic;
using WebApplication1.Models;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Views;
using WebApplication1.Presenters;
using Telerik.Web.UI;
using System.Collections;


namespace WebApplication1
{
    public partial class AdminForm : Page, INonSelectableDSView<Models.Attribute>
    {
        protected IAttributesGridPresenter attrPresenter;        
        public IEnumerable<Attribute> DataSource { get; set; }
       
        protected void Page_Load(object sender, System.EventArgs e)
        {
            attrPresenter = new AttributesGridPresenter();
            attrPresenter.Initialize(this);          
        }

 
        public void BindData()
        {                        
            gridAttr.DataSource = DataSource;            
        }

        #region gridAttr events
        protected void gridAttr_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var ddl = item.FindControl("ddlType") as RadDropDownList;

            var values = new Hashtable();
            item.ExtractValues(values);

            var attr = new Attribute()
            {
                Name = (string)values["Name"],
                TypeId = int.Parse(ddl.SelectedValue)
            };
            switch (attr.TypeId)
            {                
                case (int)AttrTypes.List:
                    var list = item.FindControl("tbListValues") as RadTextBox;
                    attrPresenter.CreateAttr(attr, list.Text.Split(new[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries).ToList());
                    break;
                default:
                    attrPresenter.CreateAttr(attr);
                    break;
            }
            
        }

        
        protected void gridAttr_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddl = item.FindControl("ddlType") as  RadDropDownList;

                var attrTypesPresenter = new AttrTypesPresenter();
                var ddlSource = new DropdownView<AttrType>() { Dropdown = ddl };
                attrTypesPresenter.Initialize(ddlSource);                
                attrTypesPresenter.LoadAttrTypes(item.DataItem as Attribute);
                
                var listsPresenter = new ListsPresenter();
                var grid = item.FindControl("gridLists") as RadGrid;                
                var gridSource = new GridView<ListValue>() { Grid = grid };
                listsPresenter.Initialize(gridSource);
                listsPresenter.LoadListValuesByAttr((int?)ViewState["attrId"]);
                ViewState["ListValues"] = gridSource.DataSource;
                grid.DataBind();
            }
        }

        protected void gridAttr_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var values = new Hashtable();
            item.ExtractValues(values);

            var attr = new Attribute()
            {
                Id = System.Convert.ToInt32(item.GetDataKeyValue("Id")),
                Name = (string)values["Name"]
            };
            attrPresenter.UpdateAttr(attr);

            var gridSource = new GridView<ListValue>() { Grid = item.FindControl("gridLists")  as RadGrid};
            var listsPresenter = this.GetListsPresenter(gridSource);
            listsPresenter.SaveChanges(attr.Id);
        }


        protected void gridAttr_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridDataItem;
            if (item == null)
                return;

            var id = System.Convert.ToInt32(item.GetDataKeyValue("Id"));
            attrPresenter.DeleteAttr(id);
        }

        protected void gridAttr_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            attrPresenter.LoadAttributes();
        }


        protected void gridAttr_ItemEvent(object sender, GridItemEventArgs e)
        {
            if (!e.Item.IsInEditMode) return;

            var attr = e.Item.DataItem as Attribute;
            if (attr == null) return;

            ViewState["attrId"] = attr.Id;

        }
        
        #endregion

        #region gridLists events

        protected IListsPresenter GetListsPresenter(INonSelectableDSView<ListValue> gridSource)
        {
            var listsPresenter = new ListsPresenter();
            listsPresenter.Initialize(gridSource, ViewState["ListValues"]);
            return listsPresenter;
        }

        protected void gridLists_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridEditableItem;
            if (item == null) return;

            var values = new Hashtable();
            item.ExtractValues(values);

            var gridSource = new GridView<ListValue>() { Grid = sender as RadGrid };
            var listsPresenter = this.GetListsPresenter(gridSource);
            listsPresenter.CreateValue(new ListValue() { Value = (string)values["Value"] });
            ViewState["ListValues"] = gridSource.DataSource;
        }

        protected void gridLists_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = e.Item as GridDataItem;
            if (item == null) return;

            var id = System.Convert.ToInt32(item.GetDataKeyValue("Id"));
            var gridSource = new GridView<ListValue>() { Grid = sender as RadGrid };
            var listsPresenter = this.GetListsPresenter(gridSource);
            listsPresenter.DeleteValue(new ListValue() { Id = id });
            ViewState["ListValues"] = gridSource.DataSource;
        }

        protected void gridLists_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            FillGridLists(sender as RadGrid);            
        }

        private void FillGridLists(RadGrid grid)
        {
            var gridSource = new GridView<ListValue>() { Grid = grid };
            var listsPresenter = GetListsPresenter(gridSource);
            var attrId = (int?)ViewState["attrId"];
            listsPresenter.LoadListValuesByAttr(attrId);
            ViewState["ListValues"] = gridSource.DataSource;
        }

        #endregion
    }
}