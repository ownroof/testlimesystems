﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace WebApplication1.Views
{
    public class GridView<T> : INonSelectableDSView<T>
        where T: class
    {      
        public IEnumerable<T> DataSource { get; set; }
        public RadGrid Grid { get; set; }

        public void BindData()
        {
            Grid.DataSource = DataSource;
        }
    }
    
}