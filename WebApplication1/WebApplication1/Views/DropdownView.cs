﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace WebApplication1.Views
{
    public class DropdownView<T> : ISelectableDSView<T>
        where T : class
    {         
        public IEnumerable<T> DataSource { get; set; }
        public int? SelectedId { get; set; }
        public RadDropDownList Dropdown { get; set; }

        public void BindData()
        {
            if (Dropdown == null)
                throw new NullReferenceException("Dropdown must be not null");

            Dropdown.DataSource = DataSource;
            Dropdown.DataBind();
            if (SelectedId.HasValue)
            {
                var item = Dropdown.FindItemByValue(SelectedId.Value.ToString());
                Dropdown.SelectedIndex = item.Index;
            }
        }
    
}
}