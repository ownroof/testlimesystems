﻿
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebApplication1.Models;

namespace WebApplication1.Views
{
    public interface IView
    {
        void BindData();
    }

    public interface ISearchClientView<T> : INonSelectableDSView<T>, IView
        where T: class
    {        
        bool IsByInn { get; set; }
        bool IsByFIO { get; set; }
        bool IsByRelatedClient { get; set; }
    }

    /// <summary>
    /// datasource for dropdowns
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISelectableDSView<T>  : INonSelectableDSView<T>, ISelectable
        where T : class
    { }

    /// <summary>
    /// datasource for grids generally
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface INonSelectableDSView<T> : IDataSourceList<T>, IView
        where T :class
    { }

    public interface IDataSourceList<T> where T : class
    {
        IEnumerable<T> DataSource { get; set; }
    }

    public interface ISelectable
    {
        int? SelectedId { get; set; }
    }

    public interface IClientFormView : IView
    {
        Client ClientConstAttrs { get; set; }
        void SaveClientProps(int clientId);
    }

}