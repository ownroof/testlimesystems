﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using WebApplication1.Models;

namespace WebApplication1.Views
{
    public class SearchClientView<TControl, TItem>: ISearchClientView<TItem>
        where TControl : RadDataBoundControl
        where TItem : class
    {
        public IEnumerable<TItem> DataSource { get; set; }        
        public bool IsByFIO { get; set; }        
        public bool IsByInn { get; set; }
        public bool IsByRelatedClient { get; set; }
        public TControl searchElement { get; set; }

        public void BindData()
        {
            searchElement.DataSource = DataSource;
            searchElement.DataBind();
        }
    }
}