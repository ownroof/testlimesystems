﻿using BLToolkit.Reflection;
using System.Collections.Generic;
using System.Linq;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    //public interface IListPropertiesGridPresenter
    //{
    //    void Initialize(INonSelectableDSView<ClientListProperty> view);
    //    void Initialize(INonSelectableDSView<ClientListProperty> view, IEnumerable<ClientListProperty> props);
    //    void Initialize(INonSelectableDSView<ClientListProperty> view, object props);
    //    void LoadProperties(int? clientId);        
    //    void SaveProperties(int clientId);
    //    void Create(ClientListProperty prop);
    //    void Update(ClientListProperty prop);
    //    void Delete(ClientListProperty prop);
    //}
    

    public class ListPropertiesGridPresenter : ClientPropsBasePresenter<ClientListProperty>
    {


        public override void Initialize(INonSelectableDSView<ClientListProperty> view)
        {
            _view = view;
            _propAccessor = TypeAccessor<ClientListPropAccessor>.CreateInstance();
        }

        //public override void Initialize(INonSelectableDSView<ClientListProperty> view, IEnumerable<ClientListProperty> props)
        //{
        //    _view = view;
        //    _propAccessor = TypeAccessor<ClientListPropAccessor>.CreateInstance();
        //    _props = new List<ClientListProperty>(props);
        //}

        //public void Initialize(INonSelectableDSView<ClientListProperty> view, object props)
        //{
        //    var list = props as IEnumerable<ClientListProperty>;
        //    if (list == null)
        //        Initialize(view);
        //    else
        //        Initialize(view, list);
        //}


        //public void LoadProperties(int? clientId) 
        //{
        //    if (_props == null)
        //    {
        //        if (clientId.HasValue)
        //            _props = _propAccessor.GetAllByClient(clientId.Value);
        //        else
        //            _props = new List<ClientListProperty>();
        //    }
        //    UpdateView();
        //}

        protected override void SaveInsertedProps(int clientId, IEnumerable<ClientListProperty> dbProps)
        {
            foreach (var memProp in _props)
            {
                var count = dbProps.Where(p => p.AttrId == memProp.AttrId && p.ValueId == memProp.ValueId).Count();
                if (count == 0)
                {
                    memProp.ClientId = clientId;
                    _propAccessor.CreateProp(memProp);
                }
            }
        }

        //public void SaveProperties(int clientId)
        //{
        //    var dbProps = _propAccessor.GetAllByClient(clientId);
        //    foreach (var dbProp in dbProps)
        //    {
        //        var memProp = _props.Find(p => p.Id == dbProp.Id);
        //        if (memProp == null)
        //            _propAccessor.DeleteProp(dbProp);
        //        else
        //            _propAccessor.UpdatePropValue(memProp);
        //    }

        //    foreach (var memProp in _props)
        //    {
        //        var count = dbProps.Where(p => p.AttrId == memProp.AttrId && p.Value == memProp.Value).Count();
        //        if (count == 0)
        //        {
        //            memProp.ClientId = clientId;
        //            _propAccessor.Create(memProp);
        //        }
        //    }
        //    UpdateView();
        //}

        //public void Create(ClientListProperty prop)
        //{
        //    _props.Add(prop);
        //    UpdateView();
        //}

        public override void Update(ClientListProperty prop)
        {
            var updated = _props.Find(p => p.Id == prop.Id && p.AttrId == prop.AttrId);
            updated.Value = prop.Value;
            updated.ValueId = prop.ValueId;
            UpdateView();
        }

        //public void Delete(ClientListProperty prop)
        //{
        //    var cnt = _props.RemoveAll(p => p.Id == prop.Id);
        //    UpdateView();
        //}

        //protected override void UpdateView()
        //{
        //    _view.DataSource = _props;
        //    _view.BindData();
        //}
    }
}