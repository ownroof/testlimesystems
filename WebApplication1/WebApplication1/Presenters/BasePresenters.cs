﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;
namespace WebApplication1.Presenters
{
    public interface IBasePresenter        
    {         
        void UpdateView();
    }

    public interface IClientPropsBasePresenter<TClientProp> : IBasePresenter
         where TClientProp : BaseClientProperty
    {
        void Initialize(INonSelectableDSView<TClientProp> view);
        void Initialize(INonSelectableDSView<TClientProp> view, IEnumerable<TClientProp> props);
        void Initialize(INonSelectableDSView<TClientProp> view, object props);
        void LoadProperties(int? clientId);
        void SaveProperties(int clientId);
        void Create(TClientProp prop);
        void Update(TClientProp prop);
        void Delete(TClientProp prop);        
    }


    public abstract class ClientPropsBasePresenter<TClientProp> : IClientPropsBasePresenter<TClientProp>
        where TClientProp : BaseClientProperty
    {
        protected INonSelectableDSView<TClientProp> _view;
        protected List<TClientProp> _props;
        protected IBaseClientPropsDataAccessor<TClientProp> _propAccessor;

        public abstract void Initialize(INonSelectableDSView<TClientProp> view);
       
        public virtual void Initialize(INonSelectableDSView<TClientProp> view, IEnumerable<TClientProp> props)
        {
            Initialize(view);
            _props = new List<TClientProp>(props);
        }

        public void Initialize(INonSelectableDSView<TClientProp> view, object props)
        {
            var list = props as IEnumerable<TClientProp>;
            if (list == null)
                Initialize(view);
            else
                Initialize(view, list);
        }

        public virtual void LoadProperties(int? clientId)
        {
            if (_props == null)
            {
                if (clientId.HasValue)
                    _props = _propAccessor.GetAllByClient(clientId.Value);
                else
                    _props = new List<TClientProp>();
            }
            UpdateView();
        }

        public virtual void SaveProperties(int clientId)
        {
            var dbProps = _propAccessor.GetAllByClient(clientId);
            SaveUpdateDeleteProps(clientId, dbProps);
            SaveInsertedProps(clientId, dbProps);


            UpdateView();
        }

        protected virtual void SaveUpdateDeleteProps(int clientId, IEnumerable<TClientProp> dbProps)
        {
            
            foreach (var dbProp in dbProps)
            {
                var memProp = _props.Find(p => p.Id == dbProp.Id);
                if (memProp == null)
                    _propAccessor.DeleteProp(dbProp);
                else
                    _propAccessor.UpdatePropValue(memProp);
            }
        }

        protected abstract void SaveInsertedProps(int clientId, IEnumerable<TClientProp> dbProps);

        public virtual void Create(TClientProp prop)
        {
            _props.Add(prop);
            UpdateView();
        }

        public abstract void Update(TClientProp prop);

        public virtual void Delete(TClientProp prop)
        {
            var cnt = _props.RemoveAll(p => p.Id == prop.Id);
            UpdateView();
        }

        public virtual void UpdateView()
        {
            _view.DataSource = _props;
            _view.BindData();
        }
    }
}