﻿using BLToolkit.Reflection;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WebApplication1.Models;
using WebApplication1.Views;
using System;

namespace WebApplication1.Presenters
{
    //public interface IStringPropertiesPresenter 
    //{
    //    void Initialize(INonSelectableDSView<ClientStringProperty> view);
    //    void Initialize(INonSelectableDSView<ClientStringProperty> view, IEnumerable<ClientStringProperty> props);
    //    void Initialize(INonSelectableDSView<ClientStringProperty> view, object props);
    //    void LoadProperties(int? clientId);
    //    void SaveProperties(int clientId);
    //    void Create(ClientStringProperty prop);
    //    void Update(ClientStringProperty prop);
    //    void Delete(ClientStringProperty prop);
    //}

    public class StringPropertiesPresenter : ClientPropsBasePresenter<ClientStringProperty>
    {
        //protected INonSelectableDSView<ClientStringProperty> _view;
        //protected ClientStringPropAccessor _propAccessor;
        //protected List<ClientStringProperty> _props;
        //protected readonly int _attrTypeId = (int)AttrTypes.String;

        public override void Initialize(INonSelectableDSView<ClientStringProperty> view)
        {
            _view = view;
            _propAccessor = TypeAccessor<ClientStringPropAccessor>.CreateInstance();
        }

        //public override void Initialize(INonSelectableDSView<ClientStringProperty> view, IEnumerable<ClientStringProperty> props)
        //{
        //    _view = view;
        //    _propAccessor = TypeAccessor<ClientStringPropAccessor>.CreateInstance();
        //    _props = new List<ClientStringProperty>(props);
        //}

        //public void Initialize(INonSelectableDSView<ClientStringProperty> view, object props)
        //{
        //    var list = props as IEnumerable<ClientStringProperty>;
        //    if (list == null)
        //        Initialize(view);
        //    else
        //        Initialize(view, list);
        //}


        //public void LoadProperties(int? clientId)
        //{
        //    if (_props == null)
        //    {
        //        if (clientId.HasValue)
        //            _props = _propAccessor.GetAllByClient(clientId.Value);
        //        else
        //            _props = new List<ClientStringProperty>();
        //    }
        //    UpdateView();
        //}

        //public void SaveProperties(int clientId)
        //{
        //    var dbProps = _propAccessor.GetAllByClient(clientId);
        //    foreach (var dbProp in dbProps)
        //    {
        //        var memProp = _props.Find(p => p.Id == dbProp.Id);
        //        if (memProp == null)
        //            _propAccessor.DeleteProp(dbProp);
        //        else
        //            _propAccessor.UpdatePropValue(memProp);
        //    }


        //    foreach (var memProp in _props)
        //    {
        //        var count = dbProps.Where(p => p.AttrId == memProp.AttrId && p.Value == memProp.Value).Count();
        //        if (count == 0)
        //        {
        //            memProp.ClientId = clientId;
        //            _propAccessor.Create(memProp);
        //        }
        //    }
        //    UpdateView();
        //}

        protected override void SaveInsertedProps(int clientId, IEnumerable<ClientStringProperty> dbProps)
        {
            foreach (var memProp in _props)
            {
                var count = dbProps.Where(p => p.AttrId == memProp.AttrId && p.Value == memProp.Value).Count();
                if (count == 0)
                {
                    memProp.ClientId = clientId;
                    _propAccessor.CreateProp(memProp);
                }
            }
        }

        //public void Create(ClientStringProperty prop)
        //{
        //    _props.Add(prop);
        //    UpdateView();
        //}

        public override void Update(ClientStringProperty prop)
        {
            var updated = _props.Find(p => p.Id == prop.Id && p.AttrId == prop.AttrId);
            updated.Value = prop.Value;
            UpdateView();
        }

        //public void Delete(ClientStringProperty prop)
        //{
        //    var cnt = _props.RemoveAll(p => p.Id == prop.Id);
        //    UpdateView();
        //}

        //protected override void UpdateView()
        //{
        //    _view.DataSource = _props;
        //    _view.BindData();
        //}

    }
}