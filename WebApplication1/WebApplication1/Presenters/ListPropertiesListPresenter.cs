﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface IListPropertiesListPresenter
    {
        void Initialize(ISelectableDSView<ClientListProperty> view);
        void LoadPropertiesByAttrId(int? attrId, int? selectedPropId);
        void LoadPropertiesByAttrId(int? attrId, int? selectedPropId, string searched);
    }

    public class ListPropertiesListPresenter : BaseSearch, IBasePresenter, IListPropertiesListPresenter
    {
        protected ISelectableDSView<ClientListProperty> _view;
        protected IEnumerable<ClientListProperty> _props;
        protected ClientListPropAccessor _propAccessor;

        public void Initialize(ISelectableDSView<ClientListProperty> view)
        {
            _view = view;
            _propAccessor = TypeAccessor<ClientListPropAccessor>.CreateInstance();
        }


        public void LoadPropertiesByAttrId(int? attrId, int? selectedPropId)
        {
            _props = _propAccessor.GetAllByAttr(attrId.Value);
            if (selectedPropId.HasValue)
                _view.SelectedId = selectedPropId;

            UpdateView();
        }

        public void LoadPropertiesByAttrId(int? attrId, int? selectedPropId, string searched)
        {
            if (IsValidSearch(searched))
                _props = _propAccessor.GetAllByAttrWithFilter(attrId.Value, searched);
            else
                _props = _propAccessor.GetAllByAttr(attrId.Value);

            if (selectedPropId.HasValue)
                _view.SelectedId = selectedPropId;

            UpdateView();
        }

        public void LoadPropertiesByAttrId(string strAttrId)
        {
            int attrId;
            if (!int.TryParse(strAttrId, out attrId))
                _props = new List<ClientListProperty>();
            else 
                _props = _propAccessor.GetAllByAttr(attrId);            
            UpdateView();
        }


        public void UpdateView()
        {
            _view.DataSource = _props;
            _view.BindData();
        }

    }
}