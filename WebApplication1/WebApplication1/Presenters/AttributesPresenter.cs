﻿using BLToolkit.Reflection;
using System.Collections.Generic;
using System.Linq;
using WebApplication1.Models;
using WebApplication1.Views;


namespace WebApplication1.Presenters
{
    public interface IAttributesGridPresenter
    {
        void Initialize(INonSelectableDSView<Attribute> view);
        void LoadAttributes();
        void CreateAttr(Attribute attr);
        void CreateAttr(Attribute attr, IEnumerable<string> values);
        void DeleteAttr(int attrId);
        void UpdateAttr(Attribute attr);
    }

    public interface IAttributesListPresenter
    {        
        void Initialize(ISelectableDSView<Attribute> view, IEnumerable<Attribute> usedAttrs);
        void LoadAttributesByType(int typeId, int? clientId);
    }

    public class AttributesGridPresenter : IAttributesGridPresenter
    {
        protected INonSelectableDSView<Attribute> _view;
        protected AttributeAccessor _attrAccessor;
        protected ListAccessor _listAccessor;

        public void Initialize(INonSelectableDSView<Attribute> view)
        {
            _view = view;
            _attrAccessor = TypeAccessor<AttributeAccessor>.CreateInstance();
            _listAccessor = TypeAccessor<ListAccessor>.CreateInstance();
        }

        public void LoadAttributes()
        {
            _view.DataSource = _attrAccessor.GetAll();
            _view.BindData();
        }

        public void CreateAttr(Attribute attr)
        {
            _attrAccessor.CreateAttr(attr);

        }

        public void CreateAttr(Attribute attr, IEnumerable<string> values)
        {
            CreateAttr(attr);            
            foreach (var value in values)
            {
                var listValue = new ListValue() { AttrId = attr.Id, Value = value };
                _listAccessor.CreateListValue(listValue);
            }
        }


        public void DeleteAttr(int attrId)
        {
            _attrAccessor.DeleteAttr(attrId);
        }

        public void UpdateAttr(Attribute attr)
        {
            _attrAccessor.UpdateAttr(attr);
        }
    }

    public class AttributesListPresenter : IAttributesListPresenter
    {
        protected ISelectableDSView<Attribute> _view;
        protected AttributeAccessor _attrAccessor;
        protected List<Attribute> _usedAttrs;

        public void Initialize(ISelectableDSView<Attribute> view, IEnumerable<Attribute> usedAttrs)
        {
            _view = view;
            _attrAccessor = TypeAccessor<AttributeAccessor>.CreateInstance();
            _usedAttrs = (usedAttrs == null) ? new List<Attribute>() : new List<Attribute>(usedAttrs);
        }

        public void LoadAttributesByType(int typeId, int? clientId, int? editingAttrId)
        {
            LoadAttributesByType(typeId, clientId);
            if (editingAttrId.HasValue)
                _view.SelectedId = editingAttrId;
        }

        public void LoadAttributesByType(int typeId, int? clientId)
        {
            var dbAttrs = (clientId.HasValue) ? _attrAccessor.GetAllByTypeAndClient(typeId, clientId.Value) : _attrAccessor.GetAllByType(typeId);
            var truncAttrs = dbAttrs.Except(_usedAttrs, new AttrComparer());
            _view.DataSource = truncAttrs;
            _view.BindData();
            _view.SelectedId = (_view.DataSource.Count() > 0) ? _view.DataSource.First().Id : (int?)null;
        }
    }

    class AttrComparer : IEqualityComparer<Attribute>
    {
        public bool Equals(Attribute a1, Attribute a2)
        {
            if (System.Object.ReferenceEquals(a1, a2)) return true;
            if (System.Object.ReferenceEquals(a1, null) || System.Object.ReferenceEquals(a2, null))
                return false;
            return a1.Id == a2.Id && a1.Name == a2.Name;
        }
        public int GetHashCode(Attribute a)
        {
            if (System.Object.ReferenceEquals(a, null)) return 0;
            int hashAttrName = a.Name.GetHashCode();
            int hashAttrId = a.Id.GetHashCode();
            return hashAttrName ^ hashAttrId;
        }
    }

}