﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface IGendersPresenter
    {
        void Initialize(INonSelectableDSView<Gender> view);
        void LoadAllGenders();
    }

    public class GendersPresenter : IGendersPresenter
    {
        protected INonSelectableDSView<Gender> _view;
        protected GenderAccessor _genders;

        public void Initialize(INonSelectableDSView<Gender> view)
        {
            _view = view;
            _genders = TypeAccessor<GenderAccessor>.CreateInstance();
        }

        public void LoadAllGenders()
        {
            _view.DataSource = _genders.GetAll();
            _view.BindData();
        }
    }
}