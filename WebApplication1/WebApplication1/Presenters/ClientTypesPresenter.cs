﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface IClientTypesPresenter
    {
        void Initialize(INonSelectableDSView<ClientType> view);
        void LoadAllClientTypes();
    }

    public class ClientTypesPresenter : IClientTypesPresenter
    {
        protected INonSelectableDSView<ClientType> _view;
        protected ClientTypeAccessor _types;

        public void Initialize(INonSelectableDSView<ClientType> view)
        {
            _view = view;
            _types = TypeAccessor<ClientTypeAccessor>.CreateInstance();
        }

        public void LoadAllClientTypes()
        {
            _view.DataSource = _types.GetAll();
            _view.BindData();
        }
    }
}