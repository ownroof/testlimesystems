﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface ISearchClientPresenter
    {
        void Initialize(ISearchClientView<Client> view);
        void SearchClients(string searched);        
    }

    public interface ISearchClientShortPresenter
    {
        void Initialize(ISearchClientView<ShortClient> view);        
        void SearchClientsLike(string searched);
    }


    public class SearchClientPresenter : BaseSearch, ISearchClientPresenter
    {
        protected ISearchClientView<Client> _view;
        protected ClientAccessor _clients;

        public void Initialize(ISearchClientView<Client> view)
        {
            _view = view;
            _clients = TypeAccessor<ClientAccessor>.CreateInstance();
        }

        public void SearchClients(string searched)
        {
            if (!IsValidSearch(searched)) return;

            if (_view.IsByInn)
                _view.DataSource = _clients.GetAllByINN(searched);
            else if (_view.IsByFIO)
                _view.DataSource = _clients.GetAllByFIO(searched);
            else if (_view.IsByRelatedClient)
                _view.DataSource = _clients.GetAllByRelatedFIO(searched);

            _view.BindData();
        }
    }
    public class SearchClientShortPresenter : BaseSearch, ISearchClientShortPresenter
    {
        protected ISearchClientView<ShortClient> _view;
        protected ClientAccessor _clients;

        public void Initialize(ISearchClientView<ShortClient> view)
        {
            _view = view;
            _clients = TypeAccessor<ClientAccessor>.CreateInstance();
        }

        public void SearchClientsLike(string searched)
        {            
            if (searched.Length>5 && IsValidSearch(searched))
                _view.DataSource = _clients.GetAllByFIOLike(searched)
                    .Select(c => new ShortClient() {
                        Id = c.Id,
                        Name = string.Join(" ", new string[] { c.Lastname, c.Firstname, c.Middlename, "ИНН:", c.INN }) });
            _view.BindData();
        }
        
    }
}