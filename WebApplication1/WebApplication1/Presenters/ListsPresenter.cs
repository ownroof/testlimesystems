﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface IListsPresenter
    {
        void Initialize(INonSelectableDSView<ListValue> view);
        void Initialize(INonSelectableDSView<ListValue> view, IEnumerable<ListValue> listValues);
        void Initialize(INonSelectableDSView<ListValue> view, object listValues);
        void LoadListValuesByAttr(int? attrId);
        void CreateValue(ListValue value);  
        void DeleteValue(ListValue value);
        void SaveChanges(int attrId);
    }

    public class ListsPresenter : IBasePresenter, IListsPresenter
    {
        protected INonSelectableDSView<ListValue> _view;        
        protected ListAccessor _listAccessor;        
        protected List<ListValue> _listValues;        

        public void Initialize(INonSelectableDSView<ListValue> view)
        {
            _view = view;
            _listAccessor = TypeAccessor<ListAccessor>.CreateInstance();            
        }
        
        public void Initialize(INonSelectableDSView<ListValue> view, IEnumerable<ListValue> listValues)
        {
            _view = view;
            _listAccessor = TypeAccessor<ListAccessor>.CreateInstance();            
            _listValues = new List<ListValue>(listValues);
        }
               
        public void Initialize(INonSelectableDSView<ListValue> view, object listValues)
        {
            var list = listValues as IEnumerable<ListValue>;
            if (list == null)
                Initialize(view);
            else
                Initialize(view, list);
        }


        public void LoadListValuesByAttr(int? attrId)
        {
           
            if (_listValues == null)
            {
                if (attrId.HasValue)
                    _listValues = _listAccessor.GetAllByAttr(attrId.Value);
                else
                    _listValues = new List<ListValue>();
            }
            UpdateView();
        }

        public void CreateValue(ListValue value)
        {
            _listValues.Add(value);
            UpdateView();
        }

        public void DeleteValue(ListValue value)
        {
            var cnt = _listValues.RemoveAll(v => v.Id == value.Id);
            UpdateView();
        }

        public void SaveChanges(int attrId)
        {
            var dbValues = _listAccessor.GetAllByAttr(attrId);
            foreach (var dbVal in dbValues)
            {
                var memVal = _listValues.Find(v => v.Id == dbVal.Id);
                if (memVal == null)
                    _listAccessor.DeleteListValue(dbVal.Id);
            }

            foreach (var memVal in _listValues)
            {
                var count = dbValues.Where(p => p.AttrId == memVal.AttrId && p.Value == memVal.Value).Count();
                if (count == 0)
                {
                    memVal.AttrId = attrId;
                    _listAccessor.CreateListValue(memVal);                    
                }
            }
            UpdateView();
        }

        public void UpdateView()
        {
            _view.DataSource = _listValues;
            _view.BindData();
        }
    }

}