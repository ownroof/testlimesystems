﻿using BLToolkit.Reflection;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface IAttrTypesPresenter
    {        
        void Initialize(ISelectableDSView<AttrType> view);        
        void LoadAttrTypes(Attribute attr);
    }

    public class AttrTypesPresenter : IAttrTypesPresenter
    {
        protected ISelectableDSView<AttrType> _view;
        protected AttrTypeAccessor _types;        

        public void Initialize(ISelectableDSView<AttrType> view)
        {
            _view = view;
            _types = TypeAccessor<AttrTypeAccessor>.CreateInstance();            
        }

        protected void LoadAttrTypes(AttrTypes selectedType)
        {
            _view.DataSource = _types.GetAll();
            _view.SelectedId = (int)selectedType;
            _view.BindData();
        }

        public void LoadAttrTypes(Attribute attr)
        {
            if (attr == null) //insert mode
                LoadAttrTypes(AttrTypes.String);
            else //edit mode
                LoadAttrTypes((AttrTypes)attr.TypeId);
        }
    }
}