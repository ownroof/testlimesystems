﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public interface IClientFormPresenter
    {
        void Initialize(IClientFormView view);
        void LoadClientMainAttrs(int? clientId);
        void SaveClientWithProps(Client client, ClientCreateHistory itemHistory);        
        //void SaveClient(Client client, ClientCreateHistory itemHistory);
    }


    public class ClientFormPresenter : IClientFormPresenter
    {
        protected IClientFormView _view;
        protected ClientAccessor _clients;
        

        public void Initialize(IClientFormView view)
        {
            _view = view;
            _clients = TypeAccessor<ClientAccessor>.CreateInstance();
        }

        public void LoadClientMainAttrs(int? clientId)
        {
            if (clientId.HasValue)
            {
                _view.ClientConstAttrs = _clients.GetById(clientId.Value);
                _view.BindData();
            }
        }

        protected void SaveClient(Client client, ClientCreateHistory itemHistory)
        {
            if (client.Id == 0)
            {
                _clients.Create(client);
                itemHistory.ClientId = client.Id;
                SaveCreateHistory(itemHistory);
            }
            else
                _clients.Update(client);
        }

        public void SaveClientWithProps(Client client, ClientCreateHistory itemHistory)
        {
            SaveClient(client, itemHistory);
            _view.SaveClientProps(client.Id);            
        }

        protected void SaveCreateHistory(ClientCreateHistory itemHistory)
        {
            var historyAccessor = TypeAccessor<ClientCreateHistoryAccessor>.CreateInstance();
            historyAccessor.Create(itemHistory);
        }

    }
}