﻿using BLToolkit.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Views;

namespace WebApplication1.Presenters
{
    public class RelatedClientPresenter : ClientPropsBasePresenter<ClientRelatedClient>
    {

        public override void Initialize(INonSelectableDSView<ClientRelatedClient> view)
        {
            _view = view;
            _propAccessor = TypeAccessor<ClientRelatedClientAccessor>.CreateInstance();
        }
      

        protected override void SaveInsertedProps(int clientId, IEnumerable<ClientRelatedClient> dbProps)
        {
            foreach (var memProp in _props)
            {
                var count = dbProps.Where(p => p.AttrId == memProp.AttrId && p.RelatedClientId == memProp.RelatedClientId).Count();
                if (count == 0)
                {
                    memProp.ClientId = clientId;
                    _propAccessor.CreateProp(memProp);
                }
            }
        }

        public override void Update(ClientRelatedClient prop)
        {
            var updated = _props.Find(p => p.Id == prop.Id && p.AttrId == prop.AttrId);
            updated.RelatedClientId = prop.RelatedClientId;
            updated.RelatedClientName = prop.RelatedClientName;
            UpdateView();
        }

    }
}