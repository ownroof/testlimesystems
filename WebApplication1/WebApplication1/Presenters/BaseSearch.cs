﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Presenters
{
    public abstract class BaseSearch
    {
        protected virtual bool IsValidSearch(string searched)
        {
            {
                return searched.IsAlphaNumerical();
            }
        }
    }
}