﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class ListValue
    {
        public int Id { get; set; }
        public int AttrId { get; set; }
        public string Value { get; set; }
    }

    //TODO: сохранять список вместе с атрибутом

    public abstract class ListAccessor : BaseDataAccessor
    {
        [SqlQuery("SELECT * FROM Lists where AttrId = @attrId")]
        public abstract List<ListValue> GetAllByAttr(int attrId);

        [SqlQuery("insert into Lists (AttrId, Value) values (@AttrId, @Value)")]
        public abstract void CreateListValue(ListValue value);

        [SqlQuery("delete from Lists where Id = @id")]
        public abstract void DeleteListValue(int id);
    }
}