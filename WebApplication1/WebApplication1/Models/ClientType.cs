﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ClientType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public abstract class ClientTypeAccessor : BaseDataAccessor
    {
        [SqlQuery(@"SELECT * from types")]
        public abstract List<ClientType> GetAll();
    }
}