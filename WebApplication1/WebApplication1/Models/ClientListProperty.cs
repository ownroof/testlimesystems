﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class ClientListProperty : BaseClientProperty
    {
        public int ValueId { get; set; }
        public string Value { get; set; }
    }

    public abstract class ClientListPropAccessor : BaseDataAccessor, IBaseClientPropsDataAccessor<ClientListProperty>
    {
        [SqlQuery(@"SELECT p.*, ca.ClientId, ca.AttrId, a.Name AS AttrName, li.Value FROM ListProperties p 
INNER JOIN dbo.ClientAttributes ca ON ca.Id = p.ClientAttrId
INNER JOIN dbo.Attributes a ON a.Id = ca.AttrId
inner join Lists li on p.ValueId = li.Id
WHERE ca.ClientId = @clientId")]
        public abstract List<ClientListProperty> GetAllByClient(int clientId);

        [SqlQuery("select * from Lists where AttrId = @attrId")]
        public abstract List<ClientListProperty> GetAllByAttr(int attrId);

        [SqlQuery("select * from Lists where AttrId = @attrId  AND value LIKE @filter+'%'")]
        public abstract List<ClientListProperty> GetAllByAttrWithFilter(int attrId, string filter);

        [SqlQuery("delete from ClientAttributes where Id = @ClientAttrId")]
        public abstract void DeleteProp(ClientListProperty prop);

        [SprocName("ListProp_Insert")]
        public abstract void CreateProp([Direction.Output("Id", "ClientAttrId")] ClientListProperty prop);

        [SqlQuery("update ListProperties set ValueId = @ValueId where Id = @Id")]
        public abstract void UpdatePropValue(ClientListProperty prop);
    }
}