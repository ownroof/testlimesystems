﻿using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace WebApplication1.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string INN { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public DateTime? Birthday { get; set; }
        public int? GenderId { get; set; }
        public int? TypeId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ShortClient
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public abstract class ClientAccessor : BaseDataAccessor
    {
        [SqlQuery("SELECT * FROM Clients")]
        public abstract List<Client> GetAll();

        [SqlQuery(@"SELECT * FROM Clients where INN = @inn")]
        public abstract List<Client> GetAllByINN(string inn);

        [SqlQuery(@"SELECT * FROM Clients 
WHERE UPPER(Lastname+Firstname+Middlename)=UPPER(REPLACE(@fio,' ',''));")]
        public abstract List<Client> GetAllByFIO(string fio);

        [SqlQuery(@"SELECT * FROM Clients 
WHERE UPPER(Lastname+Firstname+Middlename) like UPPER(REPLACE(@fio,' ',''))+'%';")]
        public abstract List<Client> GetAllByFIOLike(string fio);

        [SqlQuery(@"SELECT c.* FROM Clients as c 
left JOIN dbo.ClientAttributes AS ca ON ca.ClientId = c.Id
LEFT JOIN dbo.RelatedClient AS rc ON rc.ClientAttrId = ca.Id
LEFT JOIN dbo.Clients AS c1 ON c1.Id = rc.RelatedClientId
WHERE UPPER(c1.Lastname + c1.Firstname + c1.Middlename) = UPPER(REPLACE(@relatedFio, ' ', ''));")]
        public abstract List<Client> GetAllByRelatedFIO(string relatedFio);

        [SqlQuery(@"SELECT * FROM Clients where Id = @id")]
        public abstract Client GetById(int id);

        [SqlQuery(@"update Clients set INN = @INN, Lastname = @Lastname, Firstname = @Firstname, Middlename = @Middlename,
Birthday = @Birthday, GenderId = GenderId, TypeId = @TypeId where Id = @Id")]
        public abstract void Update(Client client);

        [SprocName("Client_Insert")]
        public abstract void Create([Direction.Output("Id")] Client client);
    }
}