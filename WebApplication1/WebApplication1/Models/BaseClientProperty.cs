﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public abstract class BaseClientProperty
    {
        public int Id { get; set; }
        public int ClientAttrId { get; set; }
        public int ClientId { get; set; }
        public int AttrId { get; set; }
        public string AttrName { get; set; }        
    }
}