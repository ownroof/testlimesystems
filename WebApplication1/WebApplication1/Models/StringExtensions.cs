﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public static class StringExtensions
    {
        public static bool IsAlphaNumerical(this string str)
        {
            foreach (var c in str)
            {
                if (!Char.IsLetterOrDigit(c) && c != ' ')
                    return false;
            }
            return true;
        }
    }
}