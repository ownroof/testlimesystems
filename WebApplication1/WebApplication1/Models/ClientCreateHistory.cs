﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ClientCreateHistory
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public DateTime Date { get; set; }
        public string Login { get; set; }
        public string Locale { get; set; }
    }

    public abstract class ClientCreateHistoryAccessor : BaseDataAccessor
    {
        [SqlQuery("insert into CreateHistory (ClientId, Login, Date, Locale) values (@ClientId, @Login, @Date, @Locale) ")]
        public abstract void Create(ClientCreateHistory itemHistory);
    }
}