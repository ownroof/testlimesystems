﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class Attribute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
    }

    public abstract class AttributeAccessor : BaseDataAccessor
    {
        [SqlQuery(@"SELECT a.Id, a.Name, a.TypeId, at.Name as Type FROM Attributes a
inner join AttrTypes at on at.Id = a.TypeId")]
        public abstract List<Attribute> GetAll();

        [SqlQuery("select * from Attributes where TypeId = @typeId")]
        public abstract List<Attribute> GetAllByType(int typeId);

        [SqlQuery(@"SELECT * FROM dbo.Attributes a 
WHERE a.TypeId = @typeId AND Id NOT IN(SELECT ca.AttrId FROM dbo.ClientAttributes ca WHERE ca.ClientId = @clientId)")]
        public abstract List<Attribute> GetAllByTypeAndClient(int typeId, int clientId);

        [SprocName("Attr_Insert")]
        public abstract void CreateAttr([Direction.Output("Id")] Attribute attr);

        [SqlQuery("delete from Attributes where Id = @id")]
        public abstract void DeleteAttr(int id);

        [SqlQuery("update Attributes set Name = @Name where Id = @Id")]
        public abstract void UpdateAttr(Attribute attr);
    }
}