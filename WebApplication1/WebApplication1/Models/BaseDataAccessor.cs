﻿using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public abstract class BaseDataAccessor : DataAccessor
    {
        protected override DbManager CreateDbManager()
        {
            var cs = new SqlConnectionStringBuilder
            {
                InitialCatalog = "TestClients2016",
                IntegratedSecurity = true,
                DataSource = @"DESKTOP-LM6M362\MSSQL"
            };
            return new DbManager(new SqlDataProvider(), cs.ConnectionString);
        }

        
    }

    public interface IBaseClientPropsDataAccessor<TClientProp>
        where TClientProp : BaseClientProperty
    {
        List<TClientProp> GetAllByClient(int clientId);        
        void DeleteProp(TClientProp prop);
        void UpdatePropValue(TClientProp memProp);
        void CreateProp(TClientProp memProp);        
    }
}