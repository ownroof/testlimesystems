﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class ClientStringProperty : BaseClientProperty
    {
        public string Value { get; set; }
    }

    public abstract class ClientStringPropAccessor : BaseDataAccessor, IBaseClientPropsDataAccessor<ClientStringProperty>
    {
        [SqlQuery(@"SELECT p.*, ca.ClientId, ca.AttrId, a.Name AS AttrName FROM dbo.StringProperties p 
INNER JOIN dbo.ClientAttributes ca ON ca.Id = p.ClientAttrId
INNER JOIN dbo.Attributes a ON a.Id = ca.AttrId
WHERE ca.ClientId = @clientId")]
        public abstract List<ClientStringProperty> GetAllByClient(int clientId);

        [SqlQuery("delete from ClientAttributes where Id = @ClientAttrId")]
        public abstract void DeleteProp(ClientStringProperty prop);

        [SprocName("StringProp_Insert")]
        public abstract void CreateProp([Direction.Output("Id", "ClientAttrId")] ClientStringProperty prop);

        [SqlQuery("update StringProperties set Value = @Value where Id = @Id")]
        public abstract void UpdatePropValue(ClientStringProperty prop);
    }
}