﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Serializable]
    public class ClientRelatedClient : BaseClientProperty
    {
        public int RelatedClientId { get; set; }
        public string RelatedClientName { get; set; }
    }

    public abstract class ClientRelatedClientAccessor : BaseDataAccessor, IBaseClientPropsDataAccessor<ClientRelatedClient>
    {
        [SqlQuery(@"sELECT c.LastName+' '+c.Firstname+' '+c.Middlename+ ' ИНН: '+c.INN AS RelatedClientName, 
p.*, ca.ClientId, ca.AttrId, a.Name AS AttrName FROM dbo.RelatedClient p 
INNER JOIN dbo.ClientAttributes ca ON ca.Id = p.ClientAttrId
INNER JOIN dbo.Attributes a ON a.Id = ca.AttrId
inner join dbo.Clients  c on c.Id = p.RelatedClientId
WHERE ca.ClientId = @clientId")]
        public abstract List<ClientRelatedClient> GetAllByClient(int clientId);

        [SqlQuery("delete from ClientAttributes where Id = @ClientAttrId")]
        public abstract void DeleteProp(ClientRelatedClient prop);

        [SprocName("RelatedClient_Insert")]
        public abstract void CreateProp([Direction.Output("Id", "ClientAttrId")] ClientRelatedClient prop);

        [SqlQuery("update RelatedClient set RelatedClientId = @RelatedClientId where Id = @Id")]
        public abstract void UpdatePropValue(ClientRelatedClient prop);
    }
}