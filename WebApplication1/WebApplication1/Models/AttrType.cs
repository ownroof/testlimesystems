﻿using BLToolkit.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class AttrType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public enum AttrTypes
    {
        String = 0,
        List = 1,
        RelatedClient = 2
    }

    public abstract class AttrTypeAccessor : BaseDataAccessor
    {
        [SqlQuery("SELECT * FROM AttrTypes")]
        public abstract List<AttrType> GetAll();

    }
}